<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'mukminin');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'OqFy[cxBFtFR(5EB0! K.y~2Jn9|*J8/)mChgiYHH#p42;q8Gya=Mn=r=JaVGM=F');
define('SECURE_AUTH_KEY',  '7;O;5M_yrNey+2 5j&c< ckZEsLvYGxgh$dR?djFW7I)I@FatH*`5LOL/4}wyS(!');
define('LOGGED_IN_KEY',    '@#$g&QCM_~aAzI6jwfpp(5Me?rbC)^Wy1r=VxPiJ4zqf40]WG~$>!Ks3BKfZ<u+<');
define('NONCE_KEY',        'N]*rmK>+r.n-~KY5:>fA%f06{2Fr4!{`88lSNK1&6H:;}iO]/tA) fCH@ZdVivkO');
define('AUTH_SALT',        '9M:a; (iKY~,Yj jfT~zLUxi(x@14`R@];BX1*&fn,+TX]VLNKp}O3*aTZBOOwJ/');
define('SECURE_AUTH_SALT', 'XKg3%e()j`5E)!G_g6X1C@`%zf5kU,*aIJtGoQ%Gc_|jm{|jp$:rN~(0),&mX`06');
define('LOGGED_IN_SALT',   '|G_<R1Va@bt3Pp*1O_-=w-k,c^ev!8dU?c!Ep6;8caHSVh%ChMapz*$>FvvvSQ3I');
define('NONCE_SALT',       'qlO1<ht4X{iBFCmf{)kvp9nf@R?zzf2!*;kX$s|E{{(CL{Rsi6D=T#^SZ+{_`1AB');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
