<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/


$first_uri = $this->uri->segment(1);
//echo $first_uri;exit;

 
$route['default_controller'] = "quran";
$route['404_override'] = '';


# quran search
$route['quran/1'] = 'quran/search_terms';

# 4/60-70
$route['(:num)/(:any)'] = 'quran/words/$1/$2';

# 4/60
$route['(:any)/'] = 'quran/words/$1';
# 4
$route['(:any)'] = 'quran/words/$1';

# 4:60
$route['(:any):(:any)/(:num)'] = 'quran/words/$1';

$route['get_aya_word/(:num)/(:any)'] = 'quran/get_aya_word/$1/$2';

# surah/4/60
$route['surah/(:num)'] = 'quran/surah/$1';
$route['surah/(:num)/(:any)'] = 'quran/surah/$1/$2';

# page/4/60
$route['page/(:num)'] = 'quran/page/$1';

# line_word/4/60
$route['line_word/(:num)'] = 'quran/aya_line_word/$1';
$route['line_word/(:num)/(:any)'] = 'quran/aya_line_word/$1/$2';

# aya_array/4/60
$route['aya_array/(:num)'] = 'quran/aya_array/$1';
$route['aya_array/(:num)/(:any)'] = 'quran/aya_array/$1/$2';

# surah_word/4/60
$route['word/(:num)'] = 'quran/some_word/$1';
$route['word/(:num)/(:any)'] = 'quran/some_word/$1/$2';

# by_word/4/60
$route['single_word/(:num)'] = 'quran/single_word/$1';
$route['single_word/(:num)/(:any)'] = 'quran/single_word/$1/$2';

# get_aya/4/70
$route['get_aya/(:num)'] = 'quran/get_aya/$1';
$route['get_aya/(:num)/(:any)'] = 'quran/get_aya/$1/$2';


# Hadits Section
$route['hadits/1'] = 'hadits/search_terms';

$route['imam'] = 'manual/imam';
# imam/1/
$route['imam/(:any)'] = 'manual/imam/$1';

# muslim/123/
$route['manual/(:any)/(:num)'] = 'manual/$1/$2';

# kitab by imam
$route['kitab'] = 'manual/kitab';

$route['kitab/(:any)'] = 'manual/kitab/$1';

# manual/bab/darimi/1
$route['bab/(:any)/(:num)'] = 'manual/bab/$1/$2';

# manual/bab/bukhari/1
//$route['bab/(:any)'] = 'manual/bab/$1';

# manual/tema/abudaud/1
$route['tema/(:any)/(:num)'] = 'manual/tema/$1/$2';

# bookmarks list_notes
$route['bookmark'] = 'save/list_notes';
$route['list_notes'] = 'save/list_notes';

/* End of file routes.php */
/* Location: ./application/config/routes.php */
