<?php

class MPage extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    
    function listPage() {
        $query = $this->db->get_where('page');
        return $query->result();
    }
    
    function getPage($slug) {
        $query = $this->db->get_where('page', array('slug'=>$slug));
        return $query->row();
    }
    
    function insertPage($data) {
        $query = $this->db->insert('page',$data);
		return $this->db->insert_id();
    }
    

}