<?php

/*
 * By Haidar Mar'ie
 * Email = coder5@ymail.com
 * msaves
 */
class MSaves extends CI_Model {

    private $sqlite;

    public static $db = DBUSE;

    private $DBUSE;
    // private TABLEUSE =
    // private $lite;
    function MSaves() {
        parent::__construct();
        $this->haditsdb = $this->load->database('sqlite', true);
    }

    function listNotes() {
        return $query = $this->haditsdb->get('notes');
    }

    function viewNote($note_id) {
        $this->haditsdb->select('*');
        $this->haditsdb->join('bab_all', 'notes.kitab_imam_id = bab_all.kitab_imam_id AND bab_all.imam_id = notes.imam_id', 'inner');
        $this->haditsdb->join('kitab_all', 'notes.kitab_imam_id = kitab_all.kitab_imam_id AND kitab_all.imam_id = notes.imam_id', 'inner');
        $query = $this->haditsdb->get_where('notes', array(
            'note_id' => $note_id
        ));
        // echo $this->haditsdb->last_query();
        return $query->row_array();
    }

    function saveNotes($docid, $notes) {
        $this->haditsdb->select('docid,*');
        $query = $this->haditsdb->get_where('had_all_fts4', array(
            'docid' => $docid
        ));
        $data = $query->row_array();
        $data['notes'] = $notes;
        $this->haditsdb->insert('notes', $data);
        print_r($data);
    }

    function saveCategory($data) {
        $this->haditsdb->insert($data);
        return $this->haditsdb->insert_id();
    }

    function insertPost($data) {
        $this->haditsdb->insert('post', $data);
        return $this->haditsdb->insert_id();
    }

}