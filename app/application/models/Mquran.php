<?php

/*
 * By Haidar Mar'ie Email = coder5@ymail.com MHadits
 * Arabic ar
 * SELECT icu_load_collation('ar_AR', 'arabic');
 */
class MQuran extends CI_Model {

    private $sqlite;

    public static $db = DBUSE;

    private $DBUSE;
    // private TABLEUSE =
    // private $lite;
    function __construct() {
        parent::__construct();
        $this->qurandb = $this->load->database('sqlite_quran', true);
        $this->mysql_quran = $this->load->database('mysql_quran', true);
        $this->quran_all = $this->load->database('quran_all', true);
    }

    function load_spellfix() {
        $db = $this->qurandb->conn_id;
        $db->loadExtension('spellfix.so');
        // $db->loadExtension('icu.so');
        return $db;
    }

    function loadArabic() {
        $sql = "SELECT icu_load_collation('ar_AR', 'arabic');";
        return $this->qurandb->query($sql);
    }

    function searchQuranBool($words, $words_min = NULL, $limit = null) {
        $this->qurandb->select('* ', FALSE);
        $this->qurandb->where("indo_text MATCH '" . $words . $words_min . "' ", NULL, FALSE);
        $msc = microtime(true);
        $this->qurandb->order_by('quran_id', 'ASC');
        $query = $this->qurandb->get('quran_fts');
        $this->firephp->log($this->qurandb->last_query());
        query_exec_time(microtime(true) - $msc);
        return $query;
    }

    function listSura() {
        $sql = "SELECT DISTINCT sura_id,sura_name,sura_name_indo,'jenis', COUNT(aya_id) as total_aya FROM quran
				GROUP BY sura_id
                ORDER BY quran_id ASC;";
        $query = $this->qurandb->query($sql);
        return $query->result();
    }

    function searchQuran($words, $words_min = NULL, $lang) {
        // $this->loadArabic();
        //$this->qurandb->where("quran_fts MATCH '" . $words . $words_min . "' ", NULL, FALSE);
        $column = columnUse($lang);
        $this->firephp->log($column);
        $this->qurandb->where($column . " MATCH  '" . $words ."'". $words_min, NULL, FALSE);
        $msc = microtime(true);
        // $this->qurandb->join('');
        $query = $this->qurandb->get('quran_fts');
        $this->firephp->log($this->qurandb->last_query());
        //echo $this->qurandb->last_query();
        query_exec_time(microtime(true) - $msc);
        return $query;
    }
    
    // SEARCH BY ARAB
    function searchQuranBoolArab($words, $words_min = NULL) {
        // $this->loadArabic();
        $this->qurandb->select('* ', FALSE);
        $this->qurandb->where("arab_simple " . $words . $words_min, NULL, FALSE);
        // $this->qurandb->where("quran_fts MATCH '" . $words . $words_min . "' ", NULL, FALSE);
        $msc = microtime(true);
        // $this->qurandb->join('');
        $query = $this->qurandb->get('quran');
        $this->firephp->log($this->qurandb->last_query());
        query_exec_time(microtime(true) - $msc);
        return $query;
    }

    function searchQuranMy($word) {
        $sql = "SELECT * FROM quran_arab_simple a
            INNER JOIN quran_indo  i ON i.quran_id = a.quran_id
            WHERE MATCH(arab_text) AGAINST('مؤمنين* ' in boolean mode)";
        $query = $this->mysql_quran->query($sql);
        // $this->mysql_quran->limit(4);
        // $query = $this->mysql_quran->get('quran_arab_simple');
        $this->firephp->log($this->mysql_quran->last_query());
        return $query;
    }

    function getSurah($sura_id, $aya_id = FALSE, $offset = FALSE) {
        $limit = 10;
        $offset = $offset ? $offset : $offset = 0;
        if (strpos($aya_id, '-') !== false) {
            $aya_split = explode('-', $aya_id);
            $aya = array("aya_id >=" => $aya_split[0],"aya_id <=" => $aya_split[1]);
            $limit = 0;
        } elseif ($aya_id) {
            $aya = $aya_id ? array('aya_id >=' => $aya_id,"aya_id <=" => $aya_id + 10) : array();
        } elseif ($aya_id == 0) {
            $aya = array();
        }
        $this->db->order_by('quran_id', 'asc');
        // $this->qurandb->limit($limit,$offset);
        $query = $this->qurandb->get_where('quran_all', array('sura_id' => $sura_id) + $aya, $limit, $offset);
        $this->firephp->log('getSurah offset ' . $offset . ' limit ' . $limit, $this->qurandb->last_query());
        return $query;
    }
    
    function getSurahName($sura_id) {
		$this->qurandb->limit(1);
		$query = $this->qurandb->get_where('quran', array('sura_id' => $sura_id));
		return $query->row();
	}

    function getSurahAll($type, $sura_id, $aya_id, $offset = FALSE) {
        $limit = 10;
        $offset = $offset ? $offset : $offset = 0;
        if (strpos($aya_id, '-') !== false) {
            $aya_split = explode('-', $aya_id);
            $aya = array("aya >=" => $aya_split[0],"aya <=" => $aya_split[1]);
            $limit = 0;
        } elseif ($aya_id) {
            $aya = $aya_id ? array('aya >=' => $aya_id,"aya <=" => $aya_id + 10) : array();
        } elseif ($aya_id == 0) {
            $aya = array();
        }
        $query = $this->qurandb->get_where('quran_' . $type, array('sura' => $sura_id) + $aya, $limit, $offset);
        fire($this->qurandb->last_query());
        return $query;
    }

    function getSurahByWord($sura_id, $aya_id = FALSE, $offset = FALSE) {
        $limit = 0;
        $offset = $offset ? $offset : $offset = 0;
        if (strpos($aya_id, '-') !== false) {
            $aya_split = explode('-', $aya_id);
            $aya = array("by_word.aya_id >=" => $aya_split[0],"by_word.aya_id <=" => $aya_split[1]);
            $this->firephp->log('aya', $aya_split);
            $limit = 0;
        } elseif ($aya_id) {
            $aya = $aya_id ? array('by_word.aya_id >=' => $aya_id,"by_word.aya_id <=" => $aya_id + 10) : array();
        } elseif ($aya_id == 0) {
            $aya = array();
        }
        $this->qurandb->join('quran', 'quran.sura_id = by_word.sura_id AND quran.aya_id = by_word.aya_id');
        // $this->qurandb->limit($limit,$offset);
        // $query = $this->qurandb->get_where('quran_indo', array('sura_id' => $sura_id) + $aya);
        $this->qurandb->order_by('no_word', 'asc');
        $query = $this->qurandb->get_where('by_word', array('by_word.sura_id' => $sura_id) + $aya);
        $this->firephp->log('getSurahByWord offset ' . $offset . ' limit ' . $limit, $this->qurandb->last_query());
        return $query;
    }

    function byWord($sura_id, $aya_id) {
        // $this->qurandb->limit(40);
        $this->qurandb->join('quran', 'quran.sura_id = by_word.sura_id AND quran.aya_id = by_word.aya_id');
        $this->qurandb->order_by('no_word', 'ASC');
        $query = $this->qurandb->get_where('by_word', array('by_word.sura_id' => $sura_id,'by_word.aya_id' => $aya_id));
        $this->firephp->log('byWord ', $this->qurandb->last_query());
        return $query;
    }

    function mushaf($sura) {
        // $this->qurandb->limit(10);
        // $query = $this->qurandb->get_where("quran", array('sura_id' => $sura,'aya_id >=' => 6,'aya_id <=' => '16'));
        $query = $this->qurandb->get_where("2_3", array('id' => 1));
        fire($this->qurandb->last_query());
        return $query->row();
    }

    function mushafTest($sura) {
        // $this->qurandb->limit(10);
        $query = $this->qurandb->get_where("quran", array('sura_id' => $sura,'aya_id >=' => 6,'aya_id <=' => '16'));
        // $query = $this->qurandb->get_where("2_3", array('id'=>1));
        fire($this->qurandb->last_query());
        return $query->result();
    }

    function mushafEdit($data, $sura) {
        $query = $this->qurandb->update('2_3', $data, array('id' => $sura));
        fire($this->qurandb->last_query());
        return $query;
    }

    function countAya($sura_id, $aya_id = FALSE) {
        if (strpos($aya_id, '-') !== false) {
            $aya_split = explode('-', $aya_id);
            $aya = array("aya_id >=" => $aya_split[0],"aya_id <=" => $aya_split[1]);
        } elseif ($aya_id) {
            $aya = $aya_id ? array('aya_id' => $aya_id) : array();
        } elseif ($aya_id == 0) {
            $aya = array();
        }
        $this->db->order_by('quran_id', 'asc');
        $this->qurandb->where(array('sura_id' => $sura_id) + $aya);
        $query = $this->qurandb->count_all_results('quran');
        $this->firephp->log($this->qurandb->last_query());
        return $query;
    }

    function getSurahMy($sura_id, $aya_id = FALSE) {
        $aya = $aya_id ? array('aya_id' => $aya_id) : array();
        $query = $this->mysql_quran->get_where('quran', array('sura_id' => $sura_id) + $aya);
        $this->firephp->log($this->qurandb->last_query());
        return $query;
    }

    function getQuranId($id) {
        $query = $this->qurandb->get_where('quran_line', array('id' => $id));
        return $query->row();
    }

    function getPageJuz() {
        $query = $this->quran_all->get_where('quran_line', array('type' => 'juz'));
        return $query->result();
    }

    function getQuranPage($page) {
        // $this->quran_all->limit(5);
        $this->quran_all->order_by('id', 'ASC');
        $query = $this->quran_all->get_where('quran_line', array('page' => $page));
        // fire($this->quran_all->last_query());
        return $query->result();
    }

    function getQuranLineIdAfter($id) {
        $query = $this->quran_all->get_where('quran_line', array('id' => $id + 1));
        return $query->row();
    }

    /**
     * Skip First Line
     *
     * @param integer $page            
     */
    function getQuranPageSkipFirst($page) {
        $this->quran_all->order_by('id', 'ASC');
        $this->quran_all->limit(18, 1);
        $query = $this->quran_all->get_where('quran_line', array('page' => $page));
        fire($this->quran_all->last_query());
        return $query->result();
    }

    function getQuranLineAya($sura_id, $aya_id) {
        $query = $this->quran_all->get_where('quran_line_aya');
        fire($this->quran_all->last_query());
        return $query->result();
    }

    function getQuranLineByID($id) {
        $query = $this->quran_all->get_where('quran_line', array('id' => $id));
        fire($this->quran_all->last_query());
        return $query;
    }

    function getSurahSimple($sura_id, $aya_id = FALSE) {
        $aya = $aya_id ? array('aya_id' => $aya_id) : array();
        $this->qurandb->join('quran_arab_simple', 'quran_id = quran.quran_id');
        $query = $this->qurandb->get_where('quran_indo', array('sura_id' => $sura_id) + $aya);
        $this->firephp->log($this->qurandb->last_query());
        return $query;
    }

    function getTranslate($sura_id, $aya_id = FALSE, $offset = FALSE) {
        $limit = 10;
        $offset = $offset ? $offset : $offset = 0;
        if (strpos($aya_id, '-') !== false) {
            $aya_split = explode('-', $aya_id);
            $aya = array("aya_id >=" => $aya_split[0],"aya_id <=" => $aya_split[1]);
            $limit = 0;
        } elseif ($aya_id) {
            $aya = $aya_id ? array('aya_id >=' => $aya_id,"aya_id <=" => $aya_id + 10) : array();
        } elseif ($aya_id == 0) {
            $aya = array();
        }
        $this->db->order_by('quran_id', 'asc');
        $query = $this->qurandb->get_where('quran', array('sura_id' => $sura_id) + $aya, $limit, $offset);
        $this->firephp->log('getSurah offset ' . $offset . ' limit ' . $limit, $this->qurandb->last_query());
        return $query->result();
    }

    function getQuranLineID($sura_id) {
        $next_sura = $sura_id + 1;
        $sql = "SELECT GROUP_CONCAT(first_last SEPARATOR '-') first_last
            	FROM 
            	(
            	 SELECT id as first_last
            	  from quran_line
            		WHERE sura_id = " . $sura_id . " OR sura_id = " . $next_sura . "
            		group by sura_id
            	) as qs";
        $query = $this->quran_all->query($sql);
        fire($this->quran_all->last_query());
        return $query->row();
    }

    function getQuranLineIDbyPage($page) {
        $next_page = $page + 1;
        $sql = "SELECT GROUP_CONCAT(first_last SEPARATOR '-') first_last
            	FROM 
            	(
            	 SELECT ql.id as first_last
        	     from quran_line ql
        		 WHERE ql.page = " . $page . " OR page = " . $next_page . "
        		 GROUP BY page
            	) as page";
        $query = $this->quran_all->query($sql);
        // fire($this->quran_all->last_query());
        return $query->row();
    }

    function getQuranLineZeroAya($sura_id, $first_id, $last_id) {
    }

    function getQuranLineAyaText($sura_id, $first_id, $last_id) {
        $this->quran_all->select("if(LOCATE('}', text)>0, SUBSTRING(text, 1, LOCATE('}', text)), text) as text", FALSE);
        $query = $this->quran_all->get_where('quran_line', array('id >' => $first_id,'id <=' => $last_id,'aya_id !=' => NULL));
        fire($this->quran_all->last_query());
        return $query->result();
    }

    function getQuranLineExactAya($sura, $aya_id) {
    }

    function getQuranLineIDbyAya($sura_id, $aya_id) {
        $before_aya = $aya_id - 1;
        $sql = "SELECT GROUP_CONCAT(first_last SEPARATOR '-') first_last
                FROM
                (
                 SELECT id as first_last
                  from quran_line ql
                	WHERE sura_id=" . $sura_id . " AND (aya_id=" . $before_aya . " OR aya_id=" . $aya_id . ")
                ) as aya";
        $query = $this->quran_all->query($sql);
        fire($this->quran_all->last_query());
        return $query->row();
    }

    function getQuranLineAyaTextRow($sura_id, $first, $last) {
        $sql = "SELECT GROUP_CONCAT(text SEPARATOR ' ') as text
                FROM 
                (
                 SELECT if(LOCATE('}', text)>0, SUBSTRING(text, 1, LOCATE('}', text)), text) as text
                  from quran_line ql
                	WHERE sura_id=" . $sura_id . " AND id >=" . $first . " AND id <=" . $last . "
                	 AND aya_id != ''
                ) as aya";
        $query = $this->quran_all->query($sql);
        fire($this->quran_all->last_query());
        return $query->row();
    }

    /**
     * if Aya_id Full in Middle
     *
     * @param unknown $sura_id            
     * @param unknown $aya_id            
     */
    function getQuranLineIDINSet($sura_id, $aya_id) {
        $sql = "SELECT id as first_last FROM quran_line
        WHERE sura_id=$sura_id
        AND FIND_IN_SET($aya_id,aya_id)";
        $query = $this->quran_all->query($sql);
        fire($this->quran_all->last_query());
        return $query->row();
    }

    /**
     * if Aya_id Full in Middle
     *
     * @param unknown $sura            
     * @param unknown $aya_id            
     * @param unknown $line_id            
     */
    function getQuranLineIDMiddle($sura, $aya_id, $line_id) {
        $before_aya_id = $aya_id - 1;
        $sql = "SELECT SUBSTRING(text,  LOCATE('{$before_aya_id}' , TRIM(text)) +3, LOCATE('{$aya_id}' , text) - LOCATE('{$before_aya_id}' , TRIM(text))  ) as text  from quran_line
                WHERE id=" . $line_id . ";";
        $query = $this->quran_all->query($sql);
        fire($this->quran_all->last_query());
        return $query->row();
    }

    function getQuranLineMiddleFull($sura_id, $aya_id) {
        $sql = "SELECT if(full_aya LIKE '%,$aya_id%',
                (SUBSTRING(text,SUBSTRING_INDEX(aya_position_3,',',1),SUBSTRING_INDEX(aya_position_3,',',-1))), (SUBSTRING(text,SUBSTRING_INDEX(aya_position_2,',',1),SUBSTRING_INDEX(aya_position_2,',',-1))) ) as text
                FROM quran_line
                WHERE sura_id=$sura_id AND FIND_IN_SET($aya_id,full_aya)";
        $query = $this->quran_all->query($sql);
        fire($this->quran_all->last_query());
        return $query->row();
    }

    /**
     * Get QuranLine Aya
     *
     * @param unknown $sura_id            
     * @param unknown $first            
     * @param string $last            
     */
    function getQuranLineIDBetweenAyaStopLast($sura_id, $first, $last = NULL) {
        $last_query = "UNION
                SELECT SUBSTRING(text, 1, LOCATE('}', text)) as first_last from quran_line
                WHERE id =" . $last . "";
        $last_id = $last ? $last_query : NULL;
        $sql = "SELECT GROUP_CONCAT(first_last SEPARATOR '') as text
                FROM (
                select substring_index(text,'}',-1) as first_last FROM quran_line
                WHERE id =" . $first . "
                $last_id
                ) as aya
            ";
        $query = $this->quran_all->query($sql);
        fire($this->quran_all->last_query());
        return $query->row();
    }

    function getQuranLineIDAyaOnly($page = NULL) {
        // $page = 'AND page =76 OR page=77';
        $page = ! empty($page) ? 'AND page =' . $page . ' ' : '';
        $sql = "SELECT id,text,sura_id,aya_id,page FROM quran_line
                WHERE non_aya=0
                 $page 
                ;";
        // die($sql);
        $query = $this->quran_all->query($sql);
        // fire($this->quran_all->last_query());
        return $query;
    }

    function getLastAya($sura_id, $page) {
        $sql = "SELECT id,aya_id_1,text,sura_id,aya_id FROM quran_line
                WHERE non_aya=0 AND page = $page AND aya_id !=0
                ORDER BY id ASC
                LIMIT 1;";
        $query = $this->quran_all->query($sql);
        // fire($this->quran_all->last_query());
        $first_aya = $query->row();
        return $first_aya->aya_id_1;
    }

    function existAyaQuran($sura_id, $aya_id) {
        $this->quran_all->limit(1);
        $query = $this->quran_all->get_where('quran_uthmani', array('sura_id' => $sura_id,'aya_id' => $aya_id));
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return FALSE;
        }
    }

    function existAya($sura_id, $aya_id, $page) {
        $this->quran_all->limit(1);
        $query = $this->quran_all->get_where('quran_line', array('sura_id' => $sura_id,'aya_id' => $aya_id,'page' => $page));
        // fire($this->quran_all->last_query());
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return FALSE;
        }
    }

    /**
     * Get Exact Aya
     * case 1 if total aya=1
     * case 2 if aya_id first and total aya>1
     * case 3 if aya_id second and total aya =2
     * case 4 if aya_id second and total aya >2
     * case 5 if aya_id third and total aya =3
     * case 6 if aya_id third and total aya =4
     * case 7 if aya_id forth and total aya =4
     *
     * @param unknown $sura_id            
     * @param unknown $aya_id            
     */
    function getQuranLineAyaMid($sura_id, $aya_id) {
        $sql = "SELECT id,line_no,page,
                (CASE
                    WHEN aya_id_1 =$aya_id AND total_aya>1 THEN SUBSTRING(text,1,aya_position_2-2)
                    WHEN aya_id_2 =$aya_id AND total_aya=2 THEN SUBSTRING(text,aya_position_2) 
                    WHEN aya_id_2 =$aya_id AND total_aya>2 THEN SUBSTRING(text,SUBSTRING_INDEX(aya_position_2,',',1),SUBSTRING_INDEX(aya_position_2,',',-1))
                    WHEN aya_id_3 =$aya_id AND total_aya=3 THEN SUBSTRING(text,aya_position_3)
                    WHEN aya_id_3 =$aya_id AND total_aya=4 THEN SUBSTRING(text,SUBSTRING_INDEX(aya_position_3,',',1),SUBSTRING_INDEX(aya_position_3,',',-1))
                    WHEN aya_id_4 =$aya_id AND total_aya=4 THEN SUBSTRING(text,aya_position_4)
                    WHEN aya_id =$aya_id THEN text
                    ELSE text
                END) AS text
                FROM quran_line WHERE sura_id=$sura_id AND FIND_IN_SET($aya_id,aya_id)";
        $query = $this->quran_all->query($sql);
        fire($this->quran_all->last_query());
        return $query->result();
    }

    /**
     * get Aya Concat
     *
     * @param unknown $sura_id            
     * @param unknown $aya            
     */
    function getQuranLineConcatAya($sura_id, $aya_id) {
        $this->setSessionConcat();
        $sql = "SELECT page,line_no,GROUP_CONCAT(text SEPARATOR '') as text
                FROM (
                    SELECT page,line_no,
                    (CASE
                        WHEN aya_id_1 =$aya_id AND total_aya>1 THEN SUBSTRING(text,1,aya_position_2-2)
                        WHEN aya_id_2 =$aya_id AND total_aya=2 THEN SUBSTRING(text,aya_position_2) 
                        WHEN aya_id_2 =$aya_id AND total_aya>2 THEN SUBSTRING(text,SUBSTRING_INDEX(aya_position_2,',',1),SUBSTRING_INDEX(aya_position_2,',',-1))
                        WHEN aya_id_3 =$aya_id AND total_aya=3 THEN SUBSTRING(text,aya_position_3)
                        WHEN aya_id_3 =$aya_id AND total_aya=4 THEN SUBSTRING(text,SUBSTRING_INDEX(aya_position_3,',',1),SUBSTRING_INDEX(aya_position_3,',',-1))
                        WHEN aya_id_4 =$aya_id AND total_aya=4 THEN SUBSTRING(text,aya_position_4)
                        WHEN aya_id =$aya_id THEN text
                    ELSE text
                    END) AS text
                    FROM quran_line WHERE sura_id=$sura_id AND FIND_IN_SET($aya_id,aya_id)
                ) as aya ";
        $query = $this->quran_all->query($sql);
        fire($this->quran_all->last_query());
        return $query->row();
    }

    function setSessionConcat() {
        $sql = "SET SESSION group_concat_max_len = 1000000;";
        $query = $this->quran_all->query($sql);
        return $query;
    }

    function getFirstLinePages() {
        $sql = "SELECT DISTINCT page, MIN(id),sura_id FROM quran_line 
                GROUP BY page 
                ORDER BY id ASC;";
        $query = $this->quran_all->query($sql);
        fire($this->quran_all->last_query());
        return $query->result();
    }

    /**
     * Get Pages $pageCount lines where sura_name at first line and bismillah second line
     */
    function getPages($pageCount) {
        $sql = "SELECT id,sura_id, COUNT(page) c,page FROM quran_line 
                GROUP BY page HAVING c = $pageCount 
                ORDER BY page ASC;";
        $query = $this->quran_all->query($sql);
        fire($this->quran_all->last_query());
        return $query->result();
    }

    /**
     * Get Pages 15 lines where sura_name at last
     */
    function getPageWrongLineNo() {
        $sql = "SELECT ql.page as page FROM (
                    SELECT page FROM quran_line
                    WHERE aya_id IS NULL AND line_no =1
                    ) ql LEFT JOIN (
                    SELECT page,text,line_no FROM quran_line
                    WHERE aya_id=0 ) b ON ql.page = b.page
                    WHERE b.page IS NULL
                    ORDER BY ql.page ASC;";
        $query = $this->quran_all->query($sql);
        fire($this->quran_all->last_query());
        return $query->result();
    }

    function getQuranLineAll() {
    }

    function getQuranLineIDZeroAya($sura_id, $aya_id) {
        $sql = "SELECT id FROM quran_line
                WHERE FIND_IN_SET($aya_id,aya_id) AND sura_id=$sura_id";
        $query = $this->quran_all->query($sql);
        return $query->result();
    }

    function getQuranPerkataIndo($sura_id, $aya_id) {
        $sql = "select group_concat(`text` separator ' ') as text
                from	
                (
                SELECT CONCAT(arab_harokat,'=',indonesia) as text 
                FROM terjemah_kata 
                WHERE sura=$sura_id AND aya=$aya_id
                ) as tk";
        $query = $this->quran_all->query($sql);
        fire($this->quran_all->last_query());
        return $query->row();
    }

    function getQuranPerkata($sura_id, $aya_id) {
        $sql = " SELECT arab_harokat as text,indo_word_text
                FROM by_word
                WHERE sura_id=$sura_id AND aya_id=$aya_id
                ORDER BY word_id ASC";
        $query = $this->qurandb->query($sql);
        fire($this->qurandb->last_query());
        return $query->result();
    }

    /**
     * for empty first aya
     *
     * @param unknown $page            
     */
    function getZeroAyaLast($page, $sura_id) {
        $this->quran_all->order_by('id', 'desc');
        $this->quran_all->limit(1);
        $query = $this->quran_all->get_where('quran_line', array('page' => $page,'aya_id' => 0,'sura_id' => $sura_id,'non_aya' => 0));
        // fire($this->quran_all->last_query());
        return $query->row();
    }

    function getFirstAya($id) {
        $query = $this->quran_all->get_where('quran_line', array('id' => $id));
        // fire($this->quran_all->last_query());
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    function getQuranSuraAya() {
        $this->quran_all->group_by('sura_id,aya_id');
        // $this->quran_all->limit(1000);
        $query = $this->quran_all->get_where('quran_uthmani');
        fire($this->quran_all->last_query());
        return $query->result();
    }
    
    function countAyaSura($sura_id) {
        $query = $this->quran_all->get_where('quran_uthmani', array('sura_id' => $sura_id));
    	return $query->num_fields();
    }

    function getQuranSuraAyaIndo($sura_id, $aya_id) {
//     	$this->qurandb->group_by('sura_id,aya_id');
    	// $this->quran_all->limit(1000);
    	$query = $this->qurandb->get_where('quran_all',array('sura_id' => $sura_id, "aya_id" => $aya_id));
    	return checkRow($query);
    	//fire($this->qurandb->last_query());
    }
    
    function getQuranWrongFirst() {
        // $this->quran_all->limit(1);
        $sql = "SELECT DISTINCT page, MIN(id) as id,aya_id,aya_id_1,sura_id,text FROM quran_line 
                WHERE non_aya=0
                GROUP BY sura_id 
                ORDER BY id ASC;";
        $query = $this->quran_all->query($sql);
        return $query->result();
    }

    function countAyaQuranLine() {
    }

    function countQuranByWord($table, $sura_id, $aya_id) {
        $this->quran_all->select("count(no_word) as no_word");
        $query = $this->quran_all->get_where($table, array('sura_id' => $sura_id,'aya_id' => $aya_id));
        return $query->row();
    }

    function getQuranAllAya() {
        $query = $this->quran_all->get_where('quran_uthmani');
        fire($this->quran_all->last_query());
        return $query->result();
    }

    function updateSura($data, $first, $last) {
        $query = $this->quran_all->update('quran_line', $data, array('id >=' => $first,'id <' => $last));
        return fire($this->quran_all->last_query());
    }

    function updateAya($data, $id) {
        $query = $this->quran_all->update('quran_line', $data, array('id' => $id));
        return $query;
    }

    function createByWord($data) {
        $query = $this->quran_all->insert('quran_by_word', $data);
        return $query;
    }

    function updatePage($data, $first, $last) {
        $query = $this->quran_all->update('quran_line', $data, array('id >=' => $first,'id <' => $last));
        return $query;
    }

    function deleteQuranLine($id) {
        return $this->quran_all->delete('quran_line', array('id' => $id));
    }

}
