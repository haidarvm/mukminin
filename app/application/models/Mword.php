<?php

/*
 * By Haidar Mar'ie Email = coder5@ymail.com Mword
 */
class MWord extends CI_Model {
	
	function __construct() {
		parent::__construct ();
	}
	
	function load_spellfix() {

	    $this->sqlite_word = $this->load->database('sqlite_word', true);
		$db = $this->sqlite_word->conn_id;
		$db->loadExtension('spellfix.so');
		return $db;
	}
	
	function get_all_word() {
		$sql = "SELECT * FROM entries LIMIT 100;";
		$query = $this->sqlite_word->query($sql);
		//$this->sqlite_word->limit(10);
		//$query = $this->sqlite_word->get('entries');
		return $query->result();
	}
	
	function insert_spell() {
	    $data['word'] = 'compatible';
	    $this->sqlite_word->insert('spelltest',$data);
	}
	
	function get_spell() {
		$this->sqlite_word->limit(5);
		$this->sqlite_word->group_by('word');
		$query = $this->sqlite_word->get('spelltest');
		return $query->result();
	}
	
	function spell_match($match){
		$sql = "SELECT * FROM spelltest WHERE word MATCH '".$match."';";
		$query = $this->sqlite_word->query($sql);
		$this->firephp->log($this->sqlite_word->last_query());
		return $query->result();
	}
	
	function likes($likes) {
		$sql = "SELECT * FROM entries WHERE word LIKE '%".$likes."%';";
		$query = $this->sqlite_word->query($sql);
		$this->firephp->log($this->sqlite_word->last_query());
		return $query->result();
	}
	
}
