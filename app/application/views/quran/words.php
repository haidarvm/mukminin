<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Quran <?php echo $surah->sura_id. '.' .$surah->sura_name_indo;?><small> Total Ayat <?php echo $countAya;?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Quran</li>
			<li class="active">Surah</li>
			<li class="active">Ayat</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-solid box-primary">
					<div class="box-header">
					   <?php //$byWord_name = $byWord->row_array()?>
						<h3 class="box-title">QS <?php //echo $byWord_name['sura_name'] . ' '. $byWord_name['sura_id'] .':'. $byWord_name['aya_id']?> <kbd>Terjemah Perkata</kbd></h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<article>
    						<?php
                            $i = 0;
//                             $current_aya = $aya_id  $countAya;
                            if($aya_id == $countAya) {
                            	$plus_aya = 0;
                            } elseif(($aya_id - $countAya) > 9) {
                            	$plus_aya = $countAya > 9 ? 9 : $countAya - 1;
                            } else {
                            	$plus_aya = 9;
                            }
//                             echo $plus_aya;
                            foreach(range($aya_id, $aya_id + $plus_aya ) as $aya) {
                                $byWord = $this->mquran->byWord($sura_id, $aya);
                                echo '<h2 class="arabic-word">';
                                foreach ($byWord->result_array() as $quran) {
                                    $docid = $quran['sura_id'];
                                    echo '<span id="by-word-'.$quran['no_word'].'"><a class="disable-color by-word" title="' . $quran["indo_word_text"] . '" data-toggle="tooltip" href="#" >' . $quran["arab_harokat"] . '</a></span>'."\n";
    			                } 
                                echo '<h2>';?>
    			                <h4 class="text-justify"><?php $translation = $byWord->first_row('array');
    			                      echo $translation['aya_id'].'. '.$translation['indo_text'];?></h4>
    			                    <?php $quran_sura = $this->mquran->getQuranSuraAyaIndo($sura_id,$translation['aya_id']);
    			                    //print_r($quran_sura);?>
		    			            <span style="text-align: left; display: none;" id="sura<?php echo $translation['aya_id']?>"><p class="arabic"><?php echo $quran_sura->arab_simple;?><br/>
"<?php echo $quran_sura->indo_text. "\" (QS ".$surah->sura_name_indo. " [".$surah->sura_id."]:".$quran_sura->aya_id.")"?></p></span>
					                <button class="btn btn-primary" onclick="copyToClipboard('#sura<?php echo $translation['aya_id']?>')">Copy Ayat</button>
	                        <?php } ?>
						</article>
					</div>
					<a id="more_button" role="button" class="btn btn-primary"> more</a>
					<div id="saveNotes" class="modal hide fade"></div>
				</div><!-- /. box -->
			</div> <!--/col-->
		</div> <!-- /.row -->
	</section> <!-- /.content -->
</div> <!-- /.content-wrapper -->
