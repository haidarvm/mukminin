<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Pencarian Quran<small>total ayat<?php //print_r($countAya)?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Hadits</li>
			<li class="active">Search</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-solid box-primary">
					<div class="box-header">
						<h3 class="box-title">Pencarian Quran</h3>
					</div>
					<div class="box-body">
						<article>
		<?php
        echo '<div class="alert alert-success">Anda Mencari Keyword = "' . $terms . '"</div>';
        echo '<div class="alert alert-info">Total Menemukan ' . $show->num_rows() . ' Ayat</div>';
        echo "<code> Took " . $_SESSION['query_exec_time'] . ' sec</code>';
        ?>
        <hr>
		<?php
        $i = 0;
        foreach ($show->result_array() as $quran) {
        $i ++;
        $docid = $quran['sura_id'];
        // echo $had->Isi_Arab. "<br/><br/>";
        $highlite_string = highlightTerms($quran["indo_text"], $terms);
        ?>
						<div class="box box-solid box-primary">
								  <div class="box-header with-border">
									<h3 class="box-title"><?php echo '<span class="quran-title">'.$quran ['sura_name'] .'</span> '. $quran['sura_id'] .':'.$quran['aya_id']?>
						            </h3>
						            <div class="box-tools pull-right">
      <!-- Buttons, labels, and many other things can be placed here! -->
      <!-- Here is a label for example -->
      <span class="label label-primary">Label</span>
    </div><!-- /.box-tools -->
								</div>
								<div class="box-body">
									<div>
            							<?php echo '<h2 class="arabic">' . highlightTerms($quran["arab_text"], $terms) .'</h2>'; ?>
            							
									</div>
									<h4><?php echo highlightTerms($quran['indo_text'], $terms)?></h4>
									<div class="btn-group" role="group">
            								<button type="button" class="btn btn-default dropdown-toggle"
            									data-toggle="dropdown" aria-expanded="false">
            									View Details <span class="caret"></span>
            								</button>
            								<ul class="dropdown-menu gundul" role="menu">
            									<li class="arabic-simple"><?php echo '<p >' . highlightTerms($quran["arab_simple"], $terms) .'</p>'; ?></li>
            									<li><a href="#">Dropdown link</a></li>
            								</ul>
        							</div>
									 <a href="#saveNotes" role="button" class="btn btn-primary">Save</a>
								</div>
							</div>
						</article>
		<?php } ?>
	    <div id="saveNotes" class="modal hide fade"></div>
					</div>
					
				</div><!-- /. box -->
				
			</div><!--/col-->
			
	   </div><!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->