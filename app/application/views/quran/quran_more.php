<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Quran <small>surat pilihan Total Ayat <?php //print_r($countAya)?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Quran</li>
			<li class="active">Surah</li>
			<li class="active">Ayat</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">

				<div class="box box-solid box-primary">
					<div class="box-header">
					   <?php echo "suraname";//$sura_name = $sura->row_array()?>
						<h3 class="box-title">QS <?php //echo $sura_name['sura_name']?> <?php //echo  $sura_name['sura_id'] .':'. $sura_name['aya_id']?></h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<article>
    						<?php
                            $text_arr = array();
                            $num = 1;
                            foreach ($quran as $row) {
                                $page = $row->page;
                                $text = explode(' ', delChar($row->text));
                                foreach (clean_aya($text) as $tex) {
                                    if (! empty(delUn($tex))) {
                                        $text_arr[] = $tex;
                                        echo 'no ' . $num ++ . ' line_no '.$row->line_no.' text ' . $tex . ' bin '. ord($tex).'</br>';
                                    }
                                }
                                echo '<h2 class="arabic">' . $row->text . '</h2>';
                                echo '<p>' . $row->line_no . '</p>';
                            }
                            echo '<pre><h3>';
                            echo 'page ' . $page . '</br>';
                            // $text_clean = array_slice($text_arr, 0, -2);
                            // $text_clean = clean_aya($text_clean);
                            // foreach($text_clean as $word) {
                            // if(!empty(delUn($word))){
                            // echo 'no '.$num++.' '.$word.'</br>';
                            // }
                            // }
                            echo '</h3></pre>';
                            ?>
						</article>
					</div>
					<a id="more_button" role="button" class="btn btn-primary"> more</a>
					<div id="saveNotes" class="modal hide fade"></div>
				</div>
				<!-- /. box -->
			</div>
			<!--/col-->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->