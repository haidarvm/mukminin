<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Quran <small>surat pilihan Total Ayat <?php echo $countAya;?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Quran</li>
			<li class="active">Surah</li>
			<li class="active">Ayat</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-solid box-primary">
					<div class="box-header">
					   <?php // $suraByWord_name = $suraByWord->first_row('array')?>
						<h3 class="box-title">QS <?php // echo $suraByWord_name['sura_name'] . ' '. $suraByWord_name['sura_id'] .':'. $suraByWord_name['aya_id']?> <kbd>Terjemah Perkata</kbd></h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<article>
    						<?php
                            $i = 0;
                            $arabByWordAya = array();
                            foreach ($suraByWord->result_array() as $quran) {
                                $arabByWordAya[$quran['aya_id']][] = '<span><a class="disable-color by-word" title="' . $quran["indo_word_text"] . '" data-toggle="tooltip" href="#" >' . $quran["arab_harokat"] . '</a></span>'; 
                            }  
                            foreach($arabByWordAya as $wordAya) {
                                echo '<h2 class="arabic-word">';
                                foreach($wordAya as $words) {
                                    echo $words,"\n"; 
                                }
                                echo '</h2>';
                                ?>
                                <h4 class="text-justify"><?php $translation = $suraByWord->first_row('array');
			                      echo $translation['indo_text'];?></h4>
		                        <?php }  ?>
						</article>
					</div>
					<a id="more_button" role="button" class="btn btn-primary"> more</a>
					<div id="saveNotes" class="modal hide fade"></div>
				</div> <!-- /. box -->
			</div> <!--/col-->
		</div> <!-- /.row -->
	</section> <!-- /.content -->
</div> <!-- /.content-wrapper -->