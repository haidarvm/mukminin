<?php
$i = 0;
$plus_aya = $countAya > 9 ? 9 : $countAya - 1;
foreach ( range($aya_id, $aya_id + $plus_aya) as $aya ) {
	$byWord = $this->mquran->byWord($sura_id, $aya);
	echo '<h2 class="arabic-word">';
	foreach ( $byWord->result_array() as $quran ) {
		$docid = $quran['sura_id'];
		echo '<span id="by-word-' . $quran['no_word'] . '"><a class="disable-color by-word" title="' . $quran["indo_word_text"] . '" data-toggle="tooltip" href="#" >' . $quran["arab_harokat"] . '</a></span>' . "\n";
	}
	echo '<h2>';
	?>
<h4 class="text-justify"><?php
	
$translation = $byWord->first_row('array');
	// $this->firephp->log($translation);
	echo $translation['aya_id'] . '. ' . $translation['indo_text'];
	?></h4>
<?php
	
$quran_sura = $this->mquran->getQuranSuraAyaIndo($sura_id, $translation['aya_id']);
	// print_r($quran_sura->aya_id); 	?>
<span style="text-align: left; display: none;" id="sura<?php echo $quran_sura->aya_id?>" style="display: none"><p lang="ar" class="artext"><?php echo $quran_sura->arab_simple;?><br />
"<?php echo $quran_sura->indo_text. "\" (QS ".$surah->sura_name_indo. " [".$sura_id."]:".$quran_sura->aya_id.")"?></p></span>
<button class="btn btn-primary" onclick="copyToClipboard('#sura<?php echo $translation['aya_id']?>')">Copy Ayat</button>
<?php } ?>