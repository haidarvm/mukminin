<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Quran <small>surat pilihan Total Ayat <?php //echo $countAya;?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Quran</li>
			<li class="active">Surah</li>
			<li class="active">Ayat</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-solid box-primary">
					<div class="box-header">
					   <?php //$byWord_name = $byWord->row_array()?>
						<h3 class="box-title">QS <?php //echo $byWord_name['sura_name'] . ' '. $byWord_name['sura_id'] .':'. $byWord_name['aya_id']?> <kbd>Terjemah Perkata</kbd></h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
					   <?php echo '<p>'.$sura->text.'</p>';?>
						<article class="arabic-big">
						  <div class="mushaf">
						    <span class="fulljustify">
    						<?php
    						
    						$text = str_replace("<br/>", "</span><span class=\"fulljustify\">", $sura->text);
                            //foreach ($sura->result() as $mushaf) {
                                echo $text;
                            ?>    
	                        <?php //} ?>
	                        </span>
	                        </div>
						</article>
					</div>
					<a id="more_button" role="button" class="btn btn-primary"> more</a>
					<div id="saveNotes" class="modal hide fade"></div>
				</div><!-- /. box -->
			</div> <!--/col-->
		</div> <!-- /.row -->
	</section> <!-- /.content -->
</div> <!-- /.content-wrapper -->