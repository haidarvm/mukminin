<div class="box box-solid box-primary">
	<div class="box-header">
		<h3 class="box-title">List Surah</h3>
	</div>
	<div class="box-body">
		<div class="table-responsive">
			<table id="listsura" class="table table-hover table-striped">
				<thead>
					<tr>
						<th style="width: 15px">No</th>
						<th>Sura</th>
						<th>Jumlah Surat</th>
					</tr>
				</thead>
				<tbody>
				<?php foreach ($listSura as $sura) { ?>
                    <tr>
						<td><?php echo $sura->sura_id;?></td>
						<td><?php echo '<a href="'. site_url() . $sura->sura_id. '/1">'. $sura->sura_name_indo .' ('. $sura->sura_name .')</a>';?></td>
						<td><?php echo $sura->total_aya;?></td>
					</tr>
                <?php }?>
				</tbody>
			</table>
		</div>
	</div>
</div> <!-- /. box -->
