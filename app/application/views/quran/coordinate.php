<script type="text/javascript"
	src="<?php echo base_url() ?>assets/js/jquery.min.js">
</script>
<style>
.sprite {
	background: url("<?php echo base_url()?>assets/img/quran/3.jpg")
		no-repeat;
}
ul.menu {
	list-style-type: none;
}
ul.menu li {
	padding: 0px;
	font-size: 16px;
	font-family: "Trebuchet MS", Arial, sans-serif;
}

ul.menu li a {
	height: 40px;
	line-height: 50px;
	display: inline-block;
	padding-left: 500px; /* To sift text off the background-image */
	color: #3E789F;
	background: url("<?php echo base_url()?>assets/img/quran/3.jpg")
		no-repeat;
	/* As all link share the same background-image */
}

ul.menu li.firefox a {
	background-position: 0 -34px;
	background-color: #ddd;
}

ul.menu li.chrome a {
	background-position: 0 -70px;
}

ul.menu li.ie a {
	background-position: 0 -200px;
}

ul.menu li.safari a {
	background-position: 0 -300px;
}

ul.menu li.opera a {
	background-position: 0 -400px;
}
</style>
<body>
	<ul class="menu">
		<li class="firefox"><a href="#">Firefox</a></li>
		<li class="chrome"><a href="#">Chrome</a></li>
		<li class="ie"><a href="#">Explorer</a></li>
		<li class="opera"><a href="#">Opera</a></li>
		<li class="safari"><a href="#">Safari</a></li>
	</ul>
<input type="text" name="regcoords" id="regcoords" />
<div id="coords">Coords</div>
<img id="divid" src="<?php echo base_url()?>assets/img/quran/3.jpg"
	style="display: block; top: 91px; left: 40px; background-color: #ddd;" />
<br />
<br />
Click to add the coordinates in this text field.
<br />


</body>
<script type="text/javascript">
/*
 Here add the ID of the HTML elements for which to show the mouse coords
 Within quotes, separated by comma.
 E.g.:   ['imgid', 'divid'];
*/
var elmids = ['divid'];

var x, y = 0;       // variables that will contain the coordinates

// Get X and Y position of the elm (from: vishalsays.wordpress.com)
function getXYpos(elm) {
  x = elm.offsetLeft;        // set x to elm’s offsetLeft
  y = elm.offsetTop;         // set y to elm’s offsetTop

  elm = elm.offsetParent;    // set elm to its offsetParent

  //use while loop to check if elm is null
  // if not then add current elm’s offsetLeft to x
  //offsetTop to y and set elm to its offsetParent
  while(elm != null) {
    x = parseInt(x) + parseInt(elm.offsetLeft);
    y = parseInt(y) + parseInt(elm.offsetTop);
    elm = elm.offsetParent;
  }

  // returns an object with "xp" (Left), "=yp" (Top) position
  return {'xp':x, 'yp':y};
}

// Get X, Y coords, and displays Mouse coordinates
function getCoords(e) {
 // coursesweb.net/
  var xy_pos = getXYpos(this);

  // if IE
  if(navigator.appVersion.indexOf("MSIE") != -1) {
    // in IE scrolling page affects mouse coordinates into an element
    // This gets the page element that will be used to add scrolling value to correct mouse coords
    var standardBody = (document.compatMode == 'CSS1Compat') ? document.documentElement : document.body;

    x = event.clientX + standardBody.scrollLeft;
    y = event.clientY + standardBody.scrollTop;
  }
  else {
    x = e.pageX;
    y = e.pageY;
  }

  x = x - xy_pos['xp'];
  y = y - xy_pos['yp'];

  // displays x and y coords in the #coords element
  document.getElementById('coords').innerHTML = 'X= '+ x+ ' ,Y= ' +y;
}

// register onmousemove, and onclick the each element with ID stored in elmids
for(var i=0; i<elmids.length; i++) {
  if(document.getElementById(elmids[i])) {
    // calls the getCoords() function when mousemove
    document.getElementById(elmids[i]).onmousemove = getCoords;

    // execute a function when click
    document.getElementById(elmids[i]).onclick = function() {
      document.getElementById('regcoords').value = x+ ' , ' +y;
    };
  }
}
</script>

<?Php
$foo_x = $x;
$foo_y = $y;
echo "X=$foo_x, Y=$foo_y ";
?>
<!-- <form action='' method=post> -->
<!-- <input type="image" alt=' Finding coordinates of an image' src="" name="foo" style=cursor:crosshair;/>-->
<!-- </form> -->