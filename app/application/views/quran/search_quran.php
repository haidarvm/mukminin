<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Quran <small>Cari / Pilih Surat</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Quran</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-6">
				<div class="box box-solid box-primary">
					<div class="box-header">
						<h3 class="box-title">Pencarian Quran Paling Lengkap & Hebat</h3>
					</div>
					<div class="box-body">
						<p>Anda Dapat Mencari Ayat Quran yg memenuhi keyword yg anda
							masukan pada pilahan 1 , Jika Anda Ingin Mengeluarkan Kata yg
							tidak di inginkan bisa masukan di input minus no.2 .</p>
						<p>
							<a href="#" class="btn btn-primary btn-large">Learn more &raquo;</a>
						</p>
						<div class="row">
							<div class="col-md-6">
								<h3>
									Bahasa Indonesia<span class="glyphicon glyphicon-search white"></span>
								</h3>
								<form class="navbar-form navbar-left" class="form-search"
									role="search" enctype="multipart/form-data"
									action="<?php echo site_url();?>quran/result/" method="POST" />
								<div class="form-group">
									<p>
										<input placeholder="Keyowrd" id="search_bool"
											name="search_bool" type="text" class="form-control" /> <input
											id="search_bool_min" placeholder="Keluarkan Keyword"
											name="search_bool_min" type="text" class="form-control" />
									</p>
									<p>
										<input type="submit" value="Search" class="btn" name="search"
											class="submit" />
									</p>

								</div>
								</form>
							</div>
							<!--/span-->
							<div class="col-md-6">
								<form action="<?php echo site_url();?>quran/result/"
									method="POST" class="form-search" />
								<div class="form-group">
									<h3>Bahasa Arab</h3>
									<p>
										<input placeholder="Arabic Search Contain Harakat"
											id="search_bool_arab" name="search_bool_arab" type="text"
											class="keyboardInput form-control" /><input
											id="search_bool_min_arab" placeholder="keluarkan kata Arab"
											name="search_bool_min_arab" type="text"
											class="keyboardInput form-control" />
									</p>
									<p>
										<input type="submit" value="SearchArab" class="btn"
											name="search" class="submit" />
									</p>
								</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Pencarian Kata / Tag</h3>
						<div class="box-tools pull-right">
							<span class="label label-primary">Tag</span>
						</div> <!-- /.box-tools -->
					</div> <!-- /.box-header -->
					<div class="box-body">The body of the box</div> <!-- /.box-body -->
					<div class="box-footer">The footer of the box</div> <!-- box-footer -->
				</div> <!-- /.box -->
			</div>
			<div class="col-md-6">
              <?php echo $listSura; # load view dashboard?> 
            </div>
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!--/row
 Jawaban Terbaik:  مؤمن= satu orang (laki2) yg beriman
مؤمنين dan مؤمنون = lbih dari 3 (laki2) yang beriman
مؤمنون= dipake kalo dia subjek (marfu' dlam ilmu nahwu)
مؤمنين= dipake kalo dia objek (manshub dlm ilmu nahwu) 
-->