<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-solid box-primary">
					<div class="box-header">
					   <?php //$byWord_name = $byWord->row_array()?>
						<h3 class="box-title">QS <?php //echo $byWord_name['sura_name'] . ' '. $byWord_name['sura_id'] .':'. $byWord_name['aya_id']?> <kbd>Terjemah
								Perkata</kbd>
						</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
					   <?php //echo '<p>'.$sura->text.'</p>';?>
						<article class="arabic">
							<div class="mushaf">
								<div class="fulljustify">
								    <div class="quran-frame-top"></div>
								    <div class="quran-frame-mid">
            						<?php
                                    foreach ($quran as $page) {
                                        echo quranLine($page->text, $page->page);
                                        ?>    
        	                        <?php } ?>
	                               </div>
								    <div class="quran-frame-bot"></div>
								</div>
							</div>
						</article>
					</div>
					<a id="bookmark" role="button" class="btn btn-primary"> bookmark</a>
					<div id="saveNotes" class="modal hide fade"></div>
				</div>
				<!-- /. box -->
			</div>
			<!--/col-->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->