<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Quran <small>surat pilihan Total Ayat <?php print_r($countAya)?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Quran</li>
			<li class="active">Surah</li>
			<li class="active">Ayat</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
			
				<div class="box box-solid box-primary">
					<div class="box-header">
					   <?php $sura_name = $sura->row_array()?>
						<h3 class="box-title">QS <?php echo $sura_name['sura_name']?> <?php echo  $sura_name['sura_id'] .':'. $sura_name['aya_id']?></h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<article>
    						<?php
                            $i = 0;
                            foreach ($sura->result_array() as $quran) {
                            $i ++;
                            $docid = $quran['sura_id'];
                            ?>
							<div>
            	            <?php echo '<h2 class="arabic">' . $quran["arab_text"] . '</h2>'; ?>
                            </div>
							<h4 class="text-justify"><?php echo $quran['aya_id'] .' '. $quran ['indo_text']?></h4>
							<!-- <a href="#haditsModal<?php echo $i; ?>" role="button" class="btn btn-default" data-toggle="modal">View Details</a> 
							<a href="#saveNotes" role="button" class="btn btn-primary">Save</a> -->
			                <?php } ?>
						</article>
					</div>
					<a id="more_button" role="button" class="btn btn-primary"> more</a>
					<div id="saveNotes" class="modal hide fade"></div>
				</div><!-- /. box -->
			</div> <!--/col-->
		</div> <!-- /.row -->
	</section> <!-- /.content -->
</div> <!-- /.content-wrapper -->