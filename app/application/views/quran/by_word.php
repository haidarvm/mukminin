<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Quran <small>surat pilihan Total Ayat <?php print_r($countAya)?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Quran</li>
			<li class="active">Surah</li>
			<li class="active">Ayat</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-solid box-primary">
					<div class="box-header">
					   <?php $byWord_name = $byWord->row_array()?>
						<h3 class="box-title">QS <?php echo $byWord_name['sura_name'] . ' '. $byWord_name['sura_id'] .':'. $byWord_name['aya_id']?> <kbd>Terjemah Perkata</kbd></h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<article>
    						<?php
                            $i = 0;
                            foreach ($byWord->result_array() as $quran) {
                                $i ++;
                                $docid = $quran['sura_id'];
                                ?>
                	            <?php echo '<span id="by-word-'.$quran['no_word'].'" class="arabic-word"><a class="disable-color by-word" title="' . $quran["indo_word_text"] . '" data-toggle="tooltip" href="#" >' . $quran["arab_harokat"] . '</a></span>'; ?>
    							<!-- <span class="text-right"><?php echo $quran['indo_word_text']?></span>-->
    							<!-- <a href="#quranModal<?php echo $i; ?>" role="button" class="btn btn-default" data-toggle="modal">View Details</a> 
    							<a href="#saveNotes" role="button" class="btn btn-primary">Save</a> -->
			                <?php } ?>
			                <h4><?php $translation = $byWord->first_row('array');
			                      $this->firephp->log($translation);
			                      echo $translation['indo_text'];?></h4>
						</article>
					</div>
					<a id="more_button" role="button" class="btn btn-primary"> more</a>
					<div id="saveNotes" class="modal hide fade"></div>
				</div><!-- /. box -->
			</div> <!--/col-->
		</div> <!-- /.row -->
	</section> <!-- /.content -->
</div> <!-- /.content-wrapper -->