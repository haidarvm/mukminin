<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Imam  <?php echo ucfirst(imam_nama($imam));?>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">hadits</li>
			<li class="active">imam</li>
			<li class="active">1</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-solid box-primary">
					<div class="box-header">
					   <?php //$imams = $sura->row_array()?>
						<h3 class="box-title">Kitab IMAM <?php //echo $sura_name['sura_name'] . ' '. $sura_name['sura_id'] .':'. $sura_name['aya_id']?></h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<article>
							<ol>
                            		<?php
                            $i = 1;
                            foreach ($kitab as $isi_kitab) {
                                
                                echo '<li class="normal-font"><a href="' . site_url() . 'bab/' . imam_slug($isi_kitab->imam_id) . '/' . $isi_kitab->kitab_imam_id . '">' . $isi_kitab->kitab_indonesia . "</a></li>";
                            }
                            ?>
		                  </ol>
						</article>
					</div>
					<a id="more_button" role="button" class="btn btn-primary"> more</a>
					<div id="saveNotes" class="modal hide fade"></div>
				</div>
				<!-- /. box -->
			</div>
			<!--/col-->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->