<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Imam  <?php echo ucfirst(imam_nama($imam));?>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">hadits</li>
			<li class="active">imam</li>
			<li class="active">1</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-solid box-primary">
					<div class="box-header">
						<h3 class="box-title">IMAM <?php echo ucfirst(imam_nama($imam));?> Kitab <?php echo $kitab->kitab_indonesia ?> </h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<article>
							<ol>
		<?php
		last_kitab($kitab->kitab_indonesia);
		$i = 1;
		foreach($bab as $isi_bab) {
			echo '<li class="normal-font"><a href="'.site_url(). 'tema/'.imam_slug($imam).'/'.$isi_bab->bab_imam_id.'">'.$isi_bab->bab_indonesia."</a></li>";
		}
		?>
		</ol>
		<p>
			<a class="btn" href="#">View details &raquo;</a>
		</p>
		<div class="span6"></div>
		<!--/span-->
	</div>
	<!--/row-->
</div>
