<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Pencarian Hadits<small>total hadits<?php //print_r($countAya)?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Hadits</li>
			<li class="active">Search</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-solid box-primary">
					<div class="box-header">
						<h3 class="box-title">Hadits Result </h3>
					</div> <!-- /.box-header -->
					<div class="box-body">
						<article>
                    		<?php
                            echo '<div class="alert alert-success">Anda Mencari Keyword = <b>"' . $terms . '"</b> Total Menemukan <code>' . $show->num_rows() . '</code> Hadits  <code>Took' . $_SESSION['query_exec_time'] . ' sec</code></div>';
                            // print_r($show);exit;
                            // echo "db use". use_db();
                            ?>
		                    <hr>
                        	<?php
                            $i = 0;
                            foreach ($show->result_array() as $had) {
                            $i ++;
                            $docid = $had['docid'];
                            // echo $had->Isi_Arab. "<br/><br/>";
                            $highlite_string = highlightTerms($had[field("isi_indonesia")], $terms);
                            $isi_arab_hadits = empty($had[field("isi_arab")]) ? highlightTerms($had[field("isi_arab_gundul")],$terms) : highlightTerms($had[field("isi_arab")],$terms);
                            ?>
									<article>
										<div class="panel panel-primary">
											<div class="panel-heading">
												<h3 class="panel-title"><?php echo $i;?>. <a
														href="<?php echo  site_url () . 'tema/' . imam_id ( $had [field ( "imam_id" )] ) . '/' . $had [field ( "bab_imam_id" )] ;?>"
														class="tooltip-bab"
														title="<?php echo  $had ['bab_indonesia'] ?>">Bab <?php echo $had ['bab_indonesia']?></a>
												</h3>
											</div>
											<div class="panel-body">
												<div>
							                         <?php echo '<h2 class="arabic-hadits">' .$isi_arab_hadits . '<h2>'; ?>
						                        </div>
                                				<?php
                                                echo '<h4>' . $highlite_string . '</h4>  
                                    			<span class="label label-danger">HR ' . imam_nama($had[field("imam_id")]) . ' No.<a title="' . $had["docid"] . '" class="nope white" data-toggle="tooltip" href="#" >' . $had[field("no_hdt")] . '</a></span>
                                    			<a title="' . $had['kitab_indonesia'] . '" href="' . site_url() . 'bab/' . imam_id($had[field("imam_id")]) . '/' . $had[field("kitab_imam_id")] . '" data-toggle="tooltip"><span class="label label-success label-kitab">Kitab ' . $had['kitab_indonesia'] . '</span></a>';
                                                ?>
			                                     <div class="btn-group" role="group">
                        								<button type="button" class="btn btn-default dropdown-toggle"
                        									data-toggle="dropdown" aria-expanded="false">
                        									View Details <span class="caret"></span>
                        								</button>
                        								<ul class="dropdown-menu gundul" role="menu">
                        								    <li>Kitab <?php echo $had['kitab_indonesia']?></li>
                        								    <li>Bab <?php echo $had['bab_indonesia']?></li>
                        									<li class="arabic-hadits"><?php echo highlightTerms($had[field("isi_arab_gundul")], $terms); ?></li>
                        									<li><a href="#">Dropdown link</a></li>
                        								</ul>
                    							</div>
												<a href="#saveNotes" role="button" class="btn btn-primary">Save</a>
											</div>
										</div>
									</article>
								<div id="haditsModal<?php echo $i;?>" class="modal fade"
									tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
									aria-hidden="true">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal"
													aria-hidden="true">x</button>
												<h3 id="myModalLabel">Hadits Details</h3>
											</div>
											<div class="modal-body">
												<h4 class="text-info"></h3>
													<h4 class="text-success"></h3>
														<p class="arabic-hadits">
					                                       
				                                        </p>
														<h4 class="isi-indo<?php echo $docid ?>">
                            				            <?php
                                                        echo highlightTerms($had[field("isi_indonesia")], $terms) . '<span class="label label-danger">HR ' . imam_nama($had[field("imam_id")]) . ' No.' . $had[field("no_hdt")] . '</span>';
                                                        ?>
                    				                    </h4>
											</div>
											<div class="modal-footer">
												<input type="hidden" name="docid<?php echo $docid;?>"
													id="docid" value="<?php echo $had['docid']?>">
												<textarea id="notes<?php echo $docid ?>"
													placeholder="Writes Your Notes to Save"></textarea>
												<button class="btn btn-default" data-dismiss="modal"
													aria-hidden="true">Close</button>
												<button onclick="saveNotes('<?php echo $docid ?>')"
													class="btn btn-primary save-notes">Save changes</button>
											</div>
										</div>
									</div>
								</div>
		                    <?php } ?>
							<div id="saveNotes" class="modal hide fade"></div>
						</article>
					</div>
					</div>
					<!-- /. box -->
				</div>
				<!--/col-->
			</div>
			<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
