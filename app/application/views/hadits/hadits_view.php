<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Imam  <?php echo ucfirst(imam_nama($imam));?>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">hadits</li>
			<li class="active">imam</li>
			<li class="active">1</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-solid box-primary">
					<div class="box-header">
						<h3 class="box-title"><?php echo imam_nama($imam)?></h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<article>
						<?php
                        echo '<h2 class="arabic">' . $hadits->c4isi_arab . '</h2>';
                        echo '<h4>' . $hadits->c5isi_indonesia . '</h4>';
                        echo '<span class="label label-danger">No.<a title="' . $hadits->c2no_hdt . '" class="nope white" data-toggle="tooltip" href="#" >' . $hadits->c2no_hdt . '</a></span>';
                        echo '<h2 class="arabic">' . $hadits->c6isi_arab_gundul . '</h2>';
                        echo "<p><code> Took " . $_SESSION['query_exec_time'] . ' sec</code></p>';
                        ?>
                		</article>
					</div>
				</div>
				<!-- /. box -->
			</div>
			<!--/col-->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
