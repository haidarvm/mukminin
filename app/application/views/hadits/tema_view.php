<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Imam  <?php echo ucfirst(imam_nama($imam));?>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">hadits</li>
			<li class="active">imam</li>
			<li class="active">1</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-solid box-primary">
					<div class="box-header">
						<h3 class="box-title"><?php echo ucfirst(imam_nama($imam));?></h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<h4><span class="text-success">Kitab <?php echo $last_kitab; ?></span></h4> 
			             <h4><span class="text-info">Bab:<?php echo $last_bab; ?></span></h4>
						<article>
							<ol>
        			         <?php
                                $i = 1;
                                echo " <code>Took " . $_SESSION['query_exec_time'] . ' sec</code>';
                                foreach ($hadits->result_array() as $isi_hadits) {
                                    $i ++;
                                    echo '<h2 class="arabic">'.$isi_hadits[field("isi_arab")].'</h2>
                                    <span class="label label-danger">No.<a title="' . $isi_hadits["docid"] . '" class="nope white" data-toggle="tooltip" href="#" >' . $isi_hadits[field("no_hdt")] . '</a></span>';
                                    echo '<h4>' . $isi_hadits[field("tema")] . ' </h4>';
                                    echo '<a href="#haditsModal' . $i . '" role="button" class="btn" data-toggle="modal">View Details</a>
                                    <hr class="divider"/>';
                                    ?>
                    			<div class="btn-group" role="group">
									<button type="button" class="btn btn-default dropdown-toggle"
										data-toggle="dropdown" aria-expanded="false">
										View Details <span class="caret"></span>
									</button>
									<ul class="dropdown-menu gundul" role="menu">
										<li>Kitab <?php //echo $had['kitab_indonesia']?></li>
										<li>Bab <?php echo $last_bab; ?></li>
										<li class="arabic-hadits"><?php echo $isi_hadits[field("isi_arab_gundul")]; ?></li>
										<li><a href="#">Dropdown link</a></li>
									</ul>
								</div>
								<a href="#saveNotes" role="button" class="btn btn-primary">Save</a>
                    			<?php } ?>
                    		</ol>
                    		</article>
				</div>
				</div>
				<!-- /. box -->
			</div>
			<!--/col-->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
