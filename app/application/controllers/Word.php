<?php
if (! defined('BASEPATH'))
    exit('No direct script access allowed');

class Word extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('mword');
        $this->mword = new MWord();
    }

    public function index() {
        $this->small('contrib');
    }
    
    public function info() {
    	echo phpinfo();
    }

    public function small($spell=FALSE) {
    	$spell = $spell ? $spell : 'monstre';
    	$data['word'] = $spell;
        $this->mword->load_spellfix();
        $data['all_word'] = $this->mword->spell_match($spell);
        $this->load->view('word', $data);
    }
    
    public function insert_spell() {
        $this->mword->insert_spell();
    }

    public function likes($likes) {
    	$data['word'] = $likes;
        $data['all_word'] = $this->mword->likes($likes);
        $this->load->view('word', $data);
    }

}
