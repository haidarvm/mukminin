<?php
if (! defined('BASEPATH'))
    exit('No direct script access allowed');

class Page extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('mpage', TRUE);
        $this->mpage = new MPage();
    }

    public function index() {
        $this->list_page();
    }
    
    public function list_page(){
        $data['title'] = "List Page";
        $data['pages'] = $this->mpage->listPage();
        $this->load->template('shop/list_page', $data);
    }
    
    public function get($slug) {
        $data['title'] = $slug;
        $data['page'] = $this->mpage->getPage($slug);
        $this->load->template('shop/page', $data);
    }
    
    public function get_id($page_id) {
        $data['title'] = "Page";
        $data['page'] = $this->mpage->getPageId($page_id);
        $this->load->template('shop/page', $data);
    }
}