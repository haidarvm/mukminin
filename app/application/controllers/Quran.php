<?php
if (! defined('BASEPATH'))
	exit('No direct script access allowed');
class Quran extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('mquran');
		$this->mquran = new MQuran();
	}

	public function index() {
		$this->search_terms();
	}

	public function search_terms() {
		$data_list['listSura'] = $this->mquran->listSura();
		$data['listSura'] = $this->load->view('quran/list_sura', $data_list, true);
		$this->load->template('quran/search_quran', $data);
	}

	public function list_sura() {
		$data['listSura'] = $this->mquran->listSura();
		$this->load->template('quran/list_sura', $data);
	}

	public function surah($sura_id, $aya_id = FALSE) {
		if (strpos($sura_id, ':' !== false)) {
			$sura_aya = explode(':', $sura_id);
			$sura_id = $sura_aya[0];
			$aya_id = $sura_aya[1];
		}
		if (empty($aya_id))
			redirect(site_url() . 'surah/' . $sura_id . '/1');
		$data['sura_id'] = $sura_id;
		if (strpos($aya_id, '-') !== false) {
			$data['aya_id'] = $aya_id;
			$aya_split = explode('-', $aya_id);
			$data['aya_id'] = $aya_split[1];
			$data['last_aya_id'] = $aya_split[1];
		} else {
			$data['last_aya_id'] = $aya_id == 1 ? $aya_id + 9 : $aya_id + 10;
		}
		$this->firephp->log('last_aya_id', $data['last_aya_id']);
		$data['sura'] = $this->mquran->getSurah($sura_id, $aya_id);
		$data['countAya'] = $this->mquran->countAya($sura_id);
		$this->load->template('quran/sura_view', $data);
	}

	/**
	 * Sura Word by word
	 *
	 * @param unknown $sura_id        	
	 * @param unknown $aya_id        	
	 */
	public function words($sura_id, $aya_id = null) {
		// echo $sura_id;exit;
		// if (preg_match('/^\d{2}:\d{2}$/', $sura_id)) {
		if (strpos($sura_id, ':') !== false) {
			$sura_aya = explode(':', $sura_id);
			$sura_id = $sura_aya[0];
			$aya_id = $sura_aya[1];
			// echo 'masuk :' . $sura_id . $aya_id; exit();
		} elseif (! is_null($sura_id) && is_null($aya_id)) {
			// echo 'masuk no aya'; exit();
			redirect(site_url() . $sura_id . '/1');
			$data['sura_id'] = $sura_id;
		} elseif (strtok($aya_id, '-') && empty($aya_id)) {
			$data['aya_id'] = $aya_id;
			$aya_split = explode('-', $aya_id);
			$data['aya_id'] = $aya_split[1];
			$data['last_aya_id'] = $aya_split[1];
		} else {
			$data['last_aya_id'] = $aya_id == 1 ? $aya_id + 9 : $aya_id + 10;
		}
		// $data['byWord'] = $this->mquran->byWord($sura_id, $aya_id);
		$data['sura_id'] = $sura_id;
		$data['aya_id'] = $aya_id;
		$data['surah'] = $this->mquran->getSurahName($sura_id);
		$data['countAya'] = $this->mquran->countAya($sura_id);
		$this->load->template('quran/words', $data);
	}

	/**
	 * Get More Aya By word
	 *
	 * @param unknown $sura_id        	
	 * @param unknown $last_aya        	
	 */
	public function get_aya_word($sura_id, $last_aya) {
		$data['aya'] = "Aya By Word";
		$data['aya_id'] = $last_aya;
		$data['sura_id'] = $sura_id;
		$data['surah'] = $this->mquran->getSurahName($sura_id);
		$data['countAya'] = $this->mquran->countAya($sura_id);
		$this->load->view('quran/aya_word', $data);
	}

	public function get_all($type, $sura_id, $aya_id) {
		$data['sura'] = $this->mquran->getSurahAll($type, $sura_id, $aya_id);
		$this->load->template('quran/sura_all_view', $data);
	}

	public function surah_word($sura_id, $aya_id = FALSE) {
		if (empty($aya_id))
			redirect(site_url() . 'surah/' . $sura_id . '/1');
		$data['sura_id'] = $sura_id;
		if (strpos($aya_id, '-') !== false) {
			$data['aya_id'] = $aya_id;
			$aya_split = explode('-', $aya_id);
			$data['first_aya'] = $aya_split[0];
			$data['aya_id'] = $aya_split[1];
			$data['last_aya_id'] = $aya_split[1];
			$data['last_aya'] = $aya_split[1];
		} else {
			$data['last_aya_id'] = $aya_id == 1 ? $aya_id + 9 : $aya_id + 10;
		}
		$this->firephp->log('last_aya_id', $data['last_aya_id']);
		$data['suraByWord'] = $this->mquran->getSurahByWord($sura_id, $aya_id);
		$data['countAya'] = $this->mquran->countAya($sura_id);
		$this->load->template('quran/sura_by_word', $data);
	}

	public function get_aya($sura_id, $last_aya) {
		$data['aya'] = "Aya";
		$data['sura'] = $this->mquran->getSurah($sura_id, $aya = FALSE, $last_aya);
		$data['countAya'] = $this->mquran->countAya($sura_id);
		$this->load->view('quran/aya', $data);
	}

	public function single_word($sura_id, $aya_id) {
		$data['byWord'] = $this->mquran->byWord($sura_id, $aya_id);
		$data['countAya'] = $this->mquran->countAya($sura_id);
		$this->load->template('quran/by_word', $data);
	}

	public function surah_simple($sura_id, $aya_id = FALSE) {
		$data['sura'] = $this->mquran->getSurahSimple($sura_id, $aya_id);
		$this->load->template('quran/sura_view', $data);
	}

	public function surah_my($sura_id, $aya_id = FALSE) {
		$data['sura'] = $this->mquran->getSurahMy($sura_id, $aya_id);
		$this->load->template('quran/sura_view', $data);
	}

	public function sura_test($sura_id, $aya_id = FALSE) {
		$data['pageTitle'] = "Sura test";
		// $data['sura'] = $this->mquran->get_one_surah($sura_id, $aya_id);
		$this->load->template('quran/quran_sura_test_view', $data);
	}

	public function mushaf($sura) {
		$data['pageTitle'] = 'Mushaf Testing';
		$data['sura'] = $this->mquran->mushaf($sura);
		$this->load->template('quran/mushaf', $data);
	}

	public function mushaf_test($sura) {
		$data['pageTitle'] = 'Mushaf Testing';
		$data['sura'] = $this->mquran->mushafTest($sura);
		$this->load->template('quran/mushaf_test', $data);
	}

	public function mushaf_edit($sura) {
		$data['pageTitle'] = 'Mushaf Testing';
		$data['sura_id'] = $sura;
		$data['sura'] = $this->mquran->mushaf($sura);
		if ($_POST) {
			$data_edit['text'] = $_POST['text'];
			$this->mquran->mushafEdit($data_edit, $sura);
		}
		$this->load->template('quran/mushaf_edit', $data);
	}

	public function translate($sura_id, $aya_id) {
		$data['pageTitle'] = 'Translate';
		$data['quran'] = $this->mquran->getTranslate($sura_id, $aya_id);
		$this->load->template('quran/translate', $data);
	}

	public function get_image($x = false, $y = false) {
		$img = imagecreatefromjpeg(basic_path() . "assets/img/quran/3.jpg");
		$w = imagesx($img);
		$h = imagesy($img);
		echo 'weight =' . $w . '</br>';
		echo 'height =' . $h . '</br>';
		$data['pageTitle'] = 'Image Quran';
		$data['x'] = $x;
		$data['y'] = $y;
		$this->load->view('quran/coordinate', $data);
	}

	public function result() {
		$data['title'] = "test";
		$post = $this->input->post(NULL, TRUE);
		$search_bool = $this->input->post('search_bool');
		
		if (! empty($post)) {
			// print_r($post);exit;
			// If Indonesia
			if ($post['search'] == 'Search') {
				if ($search_bool) {
					// $search_bool;
					$data['search'] = ($this->input->post('search_bool', TRUE));
					$text_search = delDiacritics($data['search']);
					$session_sess = search_sess(trim($text_search));
					$data['search_min'] = ($this->input->post('search_bool_min', TRUE));
					$arr = explode(' ', trim($session_sess));
					$sum = escp_db($session_sess);
					$data['terms'] = keyword($session_sess);
					$lang = isArabic($data['search']);
					// var_dump($sum);exit;
					// echo $lang; exit;
					// var_dump($is_arabic);exit;
					$data['show'] = $this->mquran->searchQuran($sum, $sum_min = FALSE, $lang);
				}
				// If Arab
			} elseif ($post['search'] == 'SearchArab') {
				$data['search'] = ($this->input->post('search_bool_arab', TRUE));
				$session_sess = search_sess($data['search']);
				$data['search2'] = ($this->input->post('search_bool_min_arab', TRUE));
				$arr = explode(' ', trim($session_sess));
				$sum = '';
				foreach ( $arr as $v ) {
					$sum .= '' . $v . '';
				}
				$data['terms'] = $data['search'];
				$data['show'] = $this->mquran->searchQuran($sum, $sum2 = "");
			}
		}
		
		// $search = $data['search'];
		$this->load->template('quran/search_quran_result', $data);
	}

	public function page_test($page) {
		$data['pageTitle'] = 'Page Shaf';
		$data['quran'] = $this->mquran->getQuranPage($page);
		$this->load->template('quran/mushaf_test', $data);
	}

	/**
	 * full
	 *
	 * @param unknown $page        	
	 */
	public function page($page) {
		$data['pageTitle'] = 'Page Shaf';
		$data['quran'] = $this->mquran->getQuranPage($page);
		$this->load->template('quran/mushaf_test', $data);
	}

	public function turn() {
		$data['pageTitle'] = 'Page Shaf';
		$this->load->template('quran/turn', $data);
	}

	/**
	 * for turn js only
	 *
	 * @param unknown $page        	
	 */
	public function page_only($page) {
		$data['quran'] = $this->mquran->getQuranPage($page);
		$this->load->view('quran/mushaf_only', $data);
	}

	/**
	 * get Quran aya Line by Sura and Aya
	 *
	 * @param unknown $sura_id        	
	 * @param unknown $aya_id        	
	 */
	public function aya_line_word($sura_id, $aya_id) {
		$data['pageTitle'] = "Sura AYA";
		// $quranID = $this->mquran->getQuranLineIDbyAya($sura_id, $aya_id);
		$data['quran'] = $this->mquran->getQuranLineAyaMid($sura_id, $aya_id);
		$this->load->template('quran/quran_more', $data);
	}

	/**
	 * extract aya word per sura
	 *
	 * @param unknown $sura_id        	
	 * @param unknown $aya_id        	
	 */
	public function aya_array($sura_id, $aya_id) {
		$data['pageTitle'] = "Quran ID";
		$data['quran'] = $this->mquran->getQuranLineConcatAya($sura_id, $aya_id);
		$this->load->template('quran/quran_word_no', $data);
	}

	public function perkata($sura_id, $aya_id) {
		$data['quran'] = $this->mquran->getQuranPerkata($sura_id, $aya_id);
		$this->load->template('quran/perkata', $data);
	}

	public function quran_id($id) {
		$data['pageTitle'] = "Quran ID";
		$data['quran'] = $this->mquran->getQuranId($id);
		$this->load->template('quran/quran_id', $data);
	}

	/**
	 * create word by word translation
	 */
	// public function create_by_word() {
	// $all_aya = $this->mquran->getQuranAllAya();
	// foreach ($all_aya as $aya) {
	// $quran = $this->mquran->getQuranLineAyaMid($aya->sura_id, $aya->aya_id);
	// $num = 1;
	// foreach ($quran as $row) {
	// $text = explode(' ', delChar($row->text));
	// foreach (clean_aya($text) as $tex) {
	// if (! empty(delUn($tex))) {
	// $no = $num ++;
	// $data['sura_id'] = $aya->sura_id;
	// $data['aya_id'] = $aya->aya_id;
	// $data['page'] = $row->page;
	// $data['line_no'] = $row->line_no;
	// $data['no_word'] = $no;
	// $data['arab_harokat'] = $tex;
	// $this->mquran->createByWord($data);
	// // echo 'no ' . $no . ' ' . $tex . '</br>';
	// }
	// }
	// }
	// }
	// }
	
	/**
	 * best update aya
	 *
	 * @param string $page        	
	 */
	public function update_aya($page = NULL) {
		// foreach (range(1, 77) as $page) {
		$ids = $this->mquran->getQuranLineIDAyaOnly($page);
		// $data['quran'] = "";
		$_SESSION['last_aya'] = '';
		foreach ( $ids->result() as $id ) {
			$line_id = $id->id;
			$sura_id = $id->sura_id;
			$pages = $id->page;
			$aya = getDigit(trim($id->text));
			$aya_first = $aya[0];
			$total_aya = $aya[1];
			$first_aya_arr = $total_aya == 1 ? $aya_first : explode(',', $aya_first);
			$aya_array = explode(',', $aya[0]);
			$count_aya = count($aya_array);
			$plus_aya = end($aya_array);
			$next_aya = $count_aya == 1 ? $aya[0] + 1 : $plus_aya + 1;
			$total_aya_plus = $aya[1] + 1;
			// echo 'aya terakhir' . end($aya_array) . ' count aya' . $count_aya . '<br/>';
			// echo $line_id. '<br/>';
			// if aya akhir
			if (substr(trim($id->text), - 4, 1) == '}') {
				// echo $line_id . ' akhir ';
				// echo $aya_first . '-' . $total_aya . '</br>';
				$_SESSION['last_aya'] = is_array($first_aya_arr) ? end($first_aya_arr) + 1 : $aya_first + 1;
				$data['aya_id'] = $aya_first;
				$data['total_aya'] = $total_aya;
				$this->mquran->updateAya($data, $line_id);
				// if middle aya
			} elseif (getDigit(trim($id->text))) {
				// echo $line_id . ' tengah ';
				// echo $aya_first . ',' . $next_aya . '-' . $total_aya_plus . '</br>';
				$_SESSION['last_aya'] = $next_aya;
				$data['aya_id'] = $aya_first . ',' . $next_aya;
				$data['total_aya'] = $total_aya_plus;
				$this->mquran->updateAya($data, $line_id);
				// if no aya number
			} else {
				// aya Empty number so much wrong
				if (! empty($_SESSION['last_aya'])) {
					// echo $line_id . ' kosong last_aya ' . $_SESSION['last_aya'] . '</br>';
					if ($this->mquran->existAyaQuran($sura_id, $_SESSION['last_aya'])) {
						$data['aya_id'] = $_SESSION['last_aya'];
						$data['total_aya'] = 1;
						$data['full_aya'] = 0;
						$this->mquran->updateAya($data, $line_id);
					} else {
						$data['aya_id'] = 1;
						$data['total_aya'] = 1;
						$data['full_aya'] = 0;
						$this->mquran->updateAya($data, $line_id);
					}
					// unset($_SESSION['last_aya']);
					// get next aya
				} else {
					// echo $line_id . ' kosong last ';
					$data['aya_id'] = 0;
					$data['total_aya'] = 1;
					$data['full_aya'] = 0;
					$this->mquran->updateAya($data, $line_id);
					$last_zero = $this->mquran->getZeroAyaLast($pages, $sura_id);
					$next_id = $last_zero->id + 1;
					$first_aya = $this->mquran->getFirstAya($next_id);
					$first_aya_arr = $first_aya->total_aya == 1 ? $first_aya->aya_id_1 : explode(',', $first_aya->aya_id);
					// $first_aya_arr = explode(',', '59,60');
					// echo is_array($first_aya_arr) ? 'end ' . end($first_aya_arr) : $first_aya->aya_id_1;
					// echo ' page ' . $first_aya->page . ' page now ' . $page . '</br>';
					if ($first_aya->page == $pages && $sura_id == $first_aya->sura_id) {
						$data['aya_id'] = $first_aya->aya_id_1 . 'elsenext';
						$this->mquran->updateAya($data, $line_id);
					}
				}
			}
		}
		// }
	}

	/**
	 * Diff with by_word quranterjemah
	 */
	public function check_word() {
		$quran = $this->mquran->getQuranSuraAya();
		foreach ( $quran as $row ) {
			$count_line = $this->mquran->countQuranByWord('quran_by_word', $row->sura_id, $row->aya_id);
			$count_word = $this->mquran->countQuranByWord('by_word', $row->sura_id, $row->aya_id);
			if ($count_line->no_word != $count_word->no_word) {
				echo $row->sura_id . ' aya ' . $row->aya_id . 'no word ' . $count_line->no_word . ' other ' . $count_word->no_word . '</br>';
			}
		}
	}

	public function update_sura() {
		// $sura = 1;
		// $i = 1;
		$one = 2;
		$two = 114;
		foreach ( range($one, $two) as $number ) {
			$quranID = $this->mquran->getQuranLineID($number);
			foreach ( $quranID as $id ) {
				$data['sura_id'] = $number;
				// $first_last = $quranID->first_last;
				$first_last = explode('-', $quranID->first_last);
				$first = $first_last[0];
				$last = $first_last[1];
				echo $number . ' = ' . $first . '-' . $last;
				$this->mquran->updateSura($data, $first, $last);
				// $data['page'] = $id->page;
				// $data['line'] = $i ++;
				// print_r($data);
				// $this->mquran->updatePage($data,$sura);
			}
		}
	}

	public function update_page() {
		// $sura = 1;
		// $i = 1;
		$one = 1;
		$two = 604;
		foreach ( range($one, $two) as $number ) {
			$quranID = $this->mquran->getQuranLineIDbyPage($number);
			foreach ( $quranID as $id ) {
				$data['page'] = $number;
				// $first_last = $quranID->first_last;
				$first_last = explode('-', $quranID->first_last);
				$first = $first_last[0];
				$last = $first_last[1];
				echo $number . ' = ' . $first . '-' . $last . '</br>';
				$this->mquran->updatePage($data, $first, $last);
				// $data['page'] = $id->page;
				// $data['line'] = $i ++;
				// print_r($data);
				// $this->mquran->updatePage($data,$sura);
			}
		}
	}

	public function update_total_aya($id) {
		$data = $this->mquran->getQuranLineByID($id);
		$str = preg_match_all('!\d+!', $data->text, $matches);
		echo '<pre>';
		// print_r($matches);
		$aya_id = implode(',', $matches[0]);
		echo $aya_id;
		echo '</br>';
		echo $str;
		echo '</pre>';
		var_dump($matches[0]);
		$datas['aya_id'] = $aya_id;
		$datas['total_aya'] = $str;
		$this->mquran->updateAya($datas, $id);
	}

	public function update_aya_total() {
		$ids = $this->mquran->getQuranLineIDAyaOnly();
		// echo $count->num_rows();
		foreach ( $ids->result() as $id ) {
			$str = preg_match_all('!\d+!', $id->text, $matches);
			$data['aya_id'] = implode(',', $matches[0]);
			$data['total_aya'] = $str;
			$this->mquran->updateAya($data, $id->id);
		}
	}

	/**
	 * Update line no where page 16
	 */
	public function update_line() {
		$page_other = $this->mquran->getPages(16);
		// $page_other = $this->mquran->getPageWrongLineNo();
		foreach ( $page_other as $page ) {
			$i = 0;
			// foreach ($this->mquran->getQuranPage($page->page) as $quran) {
			foreach ( $this->mquran->getQuranPage($page->page) as $quran ) {
				$line_no = $i ++;
				// echo $quran->id . ' page ' . $page->page . ' line no ' . $line_no . ' text '.$quran->text.'</br>';
				$data['line_no'] = $line_no;
				$this->mquran->updateAya($data, $quran->id);
			}
		}
	}

	public function update_line_page($page) {
		$i = 1;
		foreach ( $this->mquran->getQuranPage($page) as $quran ) {
			$line_no = $i ++;
			// echo $quran->id . ' page ' . $page->page . ' line no ' . $line_no . ' text '.$quran->text.'</br>';
			$data['line_no'] = $line_no;
			$this->mquran->updateAya($data, $quran->id);
		}
	}

	public function get_wrong() {
		// foreach (range(1, 200) as $page) {
		$quran = $this->mquran->getQuranWrongFirst();
		foreach ( $quran as $row ) {
			if ($row->aya_id_1 != 1) {
				echo 'id ' . $row->id . ' sura ' . $row->sura_id . ' aya ' . $row->aya_id . ' aya_id ' . $row->aya_id_1 . ' text ' . $row->text . ' page ' . $row->page . '<br/>';
				$data['aya_id'] = 1;
				// $this->mquran->updateAya($data, $row->id);
			}
		}
		// }
	}

	public function next($id) {
		$data = $this->mquran->getQuranLineByID($id);
	}

	public function get_juz() {
		$juzs = $this->mquran->getPageJuz();
		$i = 1;
		foreach ( $juzs as $juz ) {
			$no = $i ++;
			$next = $this->mquran->getQuranLineIdAfter($juz->id);
			if (! empty($juz->page) && $juz->page == $next->page) {
				$data['type'] = 'juz';
				// $this->mquran->updateAya($data,$next->id);
				$this->mquran->deleteQuranLine($next->id);
				$this->mquran->deleteQuranLine($juz->id);
				echo 'no ' . $no . ' id ' . $juz->id . ' nextId ' . $next->id . ' page ' . $juz->page . ' text' . $juz->text . ' next ' . $next->text . ' nextPage ' . $next->page . '<br/>';
			} else {
				$this->mquran->deleteQuranLine($juz->id);
				echo 'no ' . $no . ' id ' . $juz->id . ' page ' . $juz->page . ' text' . $juz->text . '<br/>';
			}
		}
	}

	function get_version() {
		echo APPPATH;
		echo CI_VERSION; // echoes something like 1.7.1
	}

	public function db() {
		echo DBUSE . '<br/>';
		if (! defined('PDO::ATTR_DRIVER_NAME')) {
			echo 'PDO unavailable';
		} elseif (defined('PDO::ATTR_DRIVER_NAME')) {
			echo 'PDO available';
		}
		phpinfo();
	}

	public function match() {
		$subject = "abcdef";
		$pattern = '/^def/';
		preg_match($pattern, substr($subject, 3), $matches, PREG_OFFSET_CAPTURE);
		print_r($matches);
		// The "i" after the pattern delimiter indicates a case-insensitive search
		if (preg_match("/php/i", "PhP is the web scripting language of choice.")) {
			echo "A match was found.";
		} else {
			echo "A match was not found.";
		}
	}

	public function test() {
		$string = "matahari barat";
		$arr = explode(' ', trim($string));
		// $words = array():
		foreach ( $arr as $v ) {
			echo '+' . $v . ' ';
			// print_r($v);
			// if (strlen($v)>0){
			// $words[] = $v;
			// }
		}
	}

	public function mailbox() {
		$this->load->template('quran/mailbox');
	}

	public function mail() {
		// $config = $this->config->item('smtp_mail');
		// print_r($config);exit;
		$this->load->library('email');
		$this->email->set_newline("\r\n");
		$this->email->to('haidarvm@gmail.com');
		$this->email->from('haidar@mukminin.com');
		$this->email->subject('Test Email From Web');
		$html = 'Haidar send to gmail vm Test Email From Web';
		$this->email->message($html);
		if ($this->email->send()) {
			echo "email has been send!\n";
			echo $html;
		} else {
			echo $this->email->print_debugger();
		}
	}

	public function test_bin() {
		$hex = 'e2808f0d';
		$remove = array('/e2808f0d0a/', '/e2808f0d/', '/e2808f/', '/@/', '/e2808f0d0a/');
		$string = preg_replace($remove, '', bin2hex($hex));
		echo $string;
	}
}

