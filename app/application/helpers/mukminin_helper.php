<?php
date_default_timezone_set('Asia/Jakarta');
$ci = & get_instance();
$ci->load->database();
$activedb = $ci->db->active_group;
// $activedb = $ci->config->item('active_group');
if ($activedb) {
    define('DBUSE', $activedb);
}
session_start();

function last_kitab($kitab = null) {
    if ($kitab) {
        $_SESSION['last_kitab'] = $kitab;
        // echo 'ke set jadi'. $kitab;
        return $kitab;
    } else {
        if (isset($_SESSION['last_kitab'])) {
            // echo 'last ny dapet';
            return $_SESSION['last_kitab'];
        } else {
            // echo 'last ny ga dapet';
            return "";
        }
    }
}

function checkRes($query) {
	if ($query->num_rows() > 0) {
		return $query->result();
	} else {
		return false;
	}
}

/**
 * Check if Result Query has one row
 *
 * @param unknown $query
 * @return boolean
 */
function checkRow($query) {
	if ($query->num_rows() > 0) {
		return $query->row();
	} else {
		return false;
	}
}

function clean_aya($text) {
    $clean = check_problem_aya($text);
    return array_values(array_filter(delChar($clean)));
}

function check_problem_aya($text) {
    $yaa = 'يَا';
    // Aw
    $key = array_search($yaa, $text);
    if ($key !== FALSE) {
        $text_fix = array($key + 1 => $yaa . '' . $text[$key + 1]);
        unset($text[$key]);
        return $text_rep = array_replace($text, $text_fix);
    } else {
        return $text;
    }
}

function delUn($text) {
    // $remove = array('/e2808f0d0a/','/e2808f0d/','/e2808f/','/@/','/e2808f0d0a/');
    $remove = array('/226/','/@/');
    $no_digit = preg_replace('/\d/', '', $text);
    $string = preg_replace($remove, '', ord($no_digit));
    return trim($string);
}

function isArabic($text) {
    if (preg_match('/\p{Arabic}/u', $text)) {
        return 'ar';
    } else {
        return 'in';
    }
}

function delDiacritics($text) {
    $remove = array('ِ','ُ','ٓ','ٰ','ْ','ٌ','ٍ','ً','ّ','َ');
    $string = str_replace($remove, '', $text);
    return $string;
}

function columnUse($lang) {
    $lang_user = $lang == 'ar' ? 'arab_simple' : 'indo_text';
    //echo $lang_user;
    return $lang_user;
}

function rangeBetween($one, $tow) {
    foreach (range($one, $two) as $number) {
        echo $number . " \n";
    }
}

function search_sess($searchterm) {
    if ($searchterm) {
        $_SESSION['searchterm'] = $searchterm;
        return $searchterm;
    } elseif ($_SESSION['searchterm']) {
        $searchterm = $_SESSION['searchterm'];
        return $searchterm;
    } else {
        $searchterm = "";
        return $searchterm;
    }
}

function type_had($type) {
    if ($type == 1) {
        return 'hadits';
    } elseif ($type == 2) {
        return "kitab";
    } elseif ($type == 3) {
        return "bab";
    }
}

function last_bab($bab = null) {
    if ($bab) {
        $_SESSION['last_bab'] = $bab;
        return $bab;
    } else {
        return $_SESSION['last_bab'];
    }
}

function use_dbs() {
    // $db = $_SESSION ['active_db'];
    // if (preg_match('/sqlite/i', $db)) {
    // return 'sqlite';
    // } else {
    // return 'default';
    // }
    return DBUSE;
}

function basic_path() {
    $fr_loc = explode('/', $_SERVER['SCRIPT_NAME']);
    $base_path = $_SERVER['DOCUMENT_ROOT'] . '/' . $fr_loc[1] . '/';
    $base_path = str_replace('index.php', '', $base_path);
    return $base_path;
}

function table_use() {
    $db = DBUSE;
    $table = $_SESSION['table_type'];
    if (preg_match('/sqlite/i', $db)) {
        if ($table == "fts") {
            $_SESSION['table_type'] = 'fts';
            return 'had_all_fts4';
        } elseif ($table == "content") {
            $_SESSION['table_type'] = 'content';
            return 'had_all_fts4_content';
        }
    } else {
        return "had_all";
    }
}

function docid() {
    $db = DBUSE;
    if (preg_match('/sqlite/i', $db)) {
        return 'docid';
    } else {
        return 'had_id';
    }
}

function delCharView($text) {
    // '{','}',
    $text = str_replace(array('@',';','<','>'), ' ', $text);
    $text = str_replace(array('{','}'), array('<b class="bracket">﴿','﴾</b>'), $text);
    return $text;
}

function delChar($text) {
    $text = str_replace(array('@',';','<','>','{','}'), ' ', $text);
    // $text = str_replace(array('{','}'), array('<b class="bracket">﴿','﴾</b>'), $text);
    return $text;
}

function arabicToNum($text) {
    $arabic = array('۰','۱','۲','۳','۴','۵','۶','۷','۸','۹');
    $num = range(0, 9);
    return str_replace($arabic, $num, $string);
}

function quranLine($text, $page) {
    if (preg_match("/\b(552|313)\b/", $page)) {
        return '<p class="mushaf-long-text">' . numToArabic(delCharView($text)) . '</p>';
    } else {
        return '<p class="mushaf-text">' . numToArabic(delCharView($text)) . '</p>';
    }
}

function numToArabic($string) {
    $standard = array("0","1","2","3","4","5","6","7","8","9");
    $east_arabic = array('٠','١','٢','٣','٤','٥','٦','٧','٨','٩','-' => '','.' => '.');
    $string = str_replace($standard, $east_arabic, $string);
    return $string;
}

function highlightNum($text_string, $num) {
    $split_words = explode(" ", $num);
    foreach ($split_words as $term) {
        $term = preg_quote($term);
        $text_string = preg_replace("/($term)/i", '<span class="highlight-terms text-danger">\1</span>', $text_string);
    }
    return $text_string;
}

function is_digit($digit){
    is_int($var);
    //return true
}

function getDigit($text) {
    $str = preg_match_all('!\d+!', $text, $matches);
    $aya_id = implode(',', $matches[0]);
    $array = array($aya_id,$str);
    return ! empty($str) ? $array : false;
}

function escp($post) {
    $arr = explode(' ', trim($post));
    $array = array_filter($arr);
    $arr_val = array_values($array);
    return $arr_val;
}

function keyword($post) {
    return implode(' ', escp($post));
}

function escp_db($post) {
    $arr_val = escp($post);
    $sum = '';
    foreach ($arr_val as $var) {
        if (preg_match('/sqlite/i', DBUSE)) {
            $sum .= ' *' . $var . '*';
        } else {
            $sum .= ' +' . $var . '*';
        }
    }
    return $sum;
}

function escp_simple_like($post) {
    $arr_val = escp($post);
    $sum = '';
    echo DBUSE;
    foreach ($arr_val as $var) {
        if (preg_match('/sqlite/i', DBUSE)) {
            $sum .= " LIKE '%" . $var . "%' AND arab_simple";
        } else {
            $sum .= ' +' . $var . '*';
        }
    }
    return rtrim($sum, " AND arab_simple");
}

function escp_arab_like($post) {
    $arr_val = escp($post);
    $sum = '';
    foreach ($arr_val as $var) {
        if (preg_match('/sqlite/i', DBUSE)) {
            $sum .= " LIKE '%" . $var . "%' AND arab_text";
        } else {
            $sum .= ' +' . $var . '*';
        }
    }
    return rtrim($sum, " AND arab_text");
}

function escp_dbmin($post) {
    $arr_val = escp($post);
    $sum = '';
    foreach ($arr_val as $var) {
        $sum .= '-' . $var . '*';
    }
    return $sum;
}

function table_use2($table) {
    $_SESSION['table_type'] = "fts";
    $db = DBUSE;
    if (preg_match('/sqlite/i', $db)) {
        $table_type = $_SESSION['table_type'];
        if (isset($table_type)) {
            if ($table == "fts") {
                $_SESSION['table_type'] = 'fts';
            } elseif ($table == "content") {
                $_SESSION['table_type'] = 'content';
            }
        }
    } else {
        $_SESSION['table_type'] = 'mysql';
    }
    // echo 'session'. $_SESSION['table_type'];
}
// Disable Debug
function debug($debug = null) {
    debug_backtrace();
    if ($debug != null) {
        $_SESSION['debug'] = "<blockquote><small>DB=" . DBUSE . " " . $debug . "</small></blockquote>";
        echo $_SESSION['debug'];
    } else {
        if (isset($_SESSION['debug'])) {
            echo $_SESSION['debug'];
        } else {
            return null;
        }
    }
}

function fire($log) {
    $ci = & get_instance();
    $ci->load->library('firephp');
    return $ci->firephp->log($log, __METHOD__);
}

function field($field) {
    $table_type = $_SESSION['table_type'];
    // echo $table_type;//die;
    if ($table_type == 'content') {
        switch ($field) {
            case "no_hdt":
                return "c2no_hdt";
            case "type":
                return "c0type";
            case "imam_id":
                return "c1imam_id";
            case "tema":
                return "c3tema";
            case "isi_arab":
                return "c4isi_arab";
            case "isi_indonesia":
                return "c5isi_indonesia";
            case "isi_arab_gundul":
                return "c6isi_arab_gundul";
            case "kitab_imam_id":
                return "c7kitab_imam_id";
            case "bab_imam_id":
                return "c8bab_imam_id";
            default:
                return 1;
        }
    } else {
        return $field;
    }
}

function colorizePerawi($text) {
    $text_rep = str_replace('[', '<span class="perawi-color">', $text);
    $text_rep2 = str_replace(']', '</span>', $text_rep);
    return $text_rep2;
}

function highlightTerms($text_string, $terms) {
    $split_words = explode(" ", $terms);
    // print_r($split_words);exit;
    // We can loop through the array of terms from string
    foreach ($split_words as $term) {
        // use preg_quote
        $term = preg_quote($term);
        // Now we can highlight the terms
        // $text_string = strtolower($text_string);
        $text_string = preg_replace("/($term)/i", '<span class="highlight-terms text-danger">\1</span>', $text_string);
    }
    // lastly, return text string with highlighted term in it
    return $text_string;
}

function highlightTermsTwo($text, $words) {
    $split_words = explode(" ", $words);
    foreach ($split_words as $term) {
        $highlighted = preg_filter('/' . preg_quote($term) . '/i', '<b><span class="text-danger">$0</span></b>', $text);
        if (! empty($highlighted)) {
            $text = $highlighted;
        }
    }
    return $text;
}

function query_exec_time($time) {
    $_SESSION['query_exec_time'] = $time;
}

function imam_id($imam_slug) {
    switch ($imam_slug) {
        case "bukhari":
            return 1;
        case "muslim":
            return 2;
        case "abudaud":
            return 3;
        case "abu-daud":
            return 3;
        case "tirmidzi":
            return 4;
        case "nasai":
            return 5;
        case "ibnumajah":
            return 6;
        case "ibnu-majah":
            return 6;
        case "ahmad":
            return 7;
        case "malik":
            return 8;
        case "darimi":
            return 9;
        default:
            return $imam_slug ? $imam_slug : false;
    }
}

function imam_slug($imam_id) {
    switch ($imam_id) {
        case "1":
            return "bukhari";
        case "2":
            return "muslim";
        case "3":
            return "abu-daud";
        case "4":
            return "tirmidzi";
        case "5":
            return "nasai";
        case "6":
            return "ibnu-majah";
        case "7":
            return "ahmad";
        case "8":
            return "malik";
        case "9":
            return "darimi";
        default:
            return $imam_id ? $imam_id : "bukhari";
    }
}

function imam_nama($imam_id) {
    switch ($imam_id) {
        case "1":
            return "Bukhari";
        case "2":
            return "Muslim";
        case "3":
            return "Abu Daud";
        case "4":
            return "Tirmidzi";
        case "5":
            return "Nasa'i";
        case "6":
            return "Ibnu Majah";
        case "7":
            return "Ahmad";
        case "8":
            return "Malik";
        case "9":
            return "Darimi";
        case "bukhari":
            return "Bukhari";
        case "muslim":
            return "Muslim";
        case "abudaud":
            return "Abu Daud";
        case "tirmidzi":
            return "Tirmidzi";
        case "nasai":
            return "Nasa'i";
        case "ibnumajah":
            return "Ibnu Majah";
        case "ahmad":
            return "Ahmad";
        case "malik":
            return "Malik";
        case "darimi":
            return "Darimi";
        default:
            return "Bukhari";
    }
}
?>
