<div class="container  bs-docs-container">
		<div class="row">
			<div class="col-md-2">
				<div class="well sidebar-nav">
					<ul class="nav">
						<li class="nav-header active"><a
							href="<?php echo site_url();?>quran"><i
								class="glyphicon glyphicon-book"></i>Qur'an</a></li>
						<li class="divider"></li>
						<li class="nav-header active"><a><i
								class="glyphicon glyphicon-book"></i> Hadits</a></li>
						<li <?php echo $uri =='bukhari'?'class="active"':''; ?>><a
							href="<?php echo site_url();?>kitab/bukhari">Shahih Bukhari</a></li>
						<li <?php echo $uri =='muslim'?'class="active"':''; ?>><a
							href="<?php echo site_url();?>kitab/muslim">Shahih Muslim</a></li>
						<li <?php echo $uri =='abudaud'?'class="active"':''; ?>><a
							href="<?php echo site_url();?>kitab/abudaud">Sunan Abu Daud</a></li>
						<li <?php echo $uri =='tirmidzi'?'class="active"':''; ?>><a
							href="<?php echo site_url();?>kitab/tirmidzi">Sunan Tirmidzi</a></li>
						<li <?php echo $uri =='nasai'?'class="active"':''; ?>><a
							href="<?php echo site_url();?>kitab/nasai">Sunan Nasa'i</a></li>
						<li <?php echo $uri =='ibnumajah'?'class="active"':''; ?>><a
							href="<?php echo site_url();?>kitab/ibnumajah">Sunan Ibnu Majah</a></li>
						<li <?php echo $uri =='ahmad'?'class="active"':''; ?>><a
							href="<?php echo site_url();?>kitab/ahmad">Musnad Ahmad</a></li>
						<li <?php echo $uri =='malik'?'class="active"':''; ?>><a
							href="<?php echo site_url();?>kitab/malik">Muwatha' Malik</a></li>
						<li <?php echo $uri =='darimi'?'class="active"':''; ?>><a
							href="<?php echo site_url();?>kitab/darimi">Sunan Darimi</a></li>
					</ul>
				</div>
				<!--/.well -->
			</div>
			<!--/span-->
