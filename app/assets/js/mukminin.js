$(function() {
	$("#example").dataTable();
	$('#listsura').dataTable({
		"iDisplayLength" : 50
	});
	$('#example2').dataTable({
		"bPaginate" : true,
		"bLengthChange" : false,
		"bFilter" : false,
		"bSort" : true,
		"bInfo" : true,
		"bAutoWidth" : false
	});
});

$(document).ready(function() {
	theWidth = $(".kata .arab-perkata").css('width');
	$(".kata").css('width', theWidth);
});



function copyToClipboard(arabic) {
	var $temp = $("<textarea>");
	$("body").append($temp);
	var arabictext = $(arabic).text(); 
	$temp.val(arabictext).select();
	document.execCommand("copy");
	$temp.remove();
}
